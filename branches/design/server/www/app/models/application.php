<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The Application model
 *
 * An Application is a generic, versionless office application, like Abiword or Microsoft Excel
 */
class Application extends AppModel
{
	/** @var string Each application has many instances (workers) running on factories */
	public $hasMany = 'Worker';

	/** @var string Every application supports a certain ODF doctype */
	public $hasAndBelongsToMany = 'Doctype';

	/** @var string the default order */
	public $order = 'Application.name ASC';
}

?>
