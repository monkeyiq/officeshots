<div class="factories form">
<?php echo $form->create('Factory');?>
	<fieldset>
 		<legend><?php __('Add Factory');?></legend>
	<?php
		echo $form->input('user_id');
		echo $form->input('operatingsystem_id');
		echo $form->input('name');
		echo $form->input('hardware');
		echo $form->input('last_poll');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Factories', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Operatingsystems', true), array('controller'=> 'operatingsystems', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Operatingsystem', true), array('controller'=> 'operatingsystems', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Workers', true), array('controller'=> 'workers', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Worker', true), array('controller'=> 'workers', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Results', true), array('controller'=> 'results', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Result', true), array('controller'=> 'results', 'action'=>'add')); ?> </li>
	</ul>
</div>
