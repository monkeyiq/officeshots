<div class="workers form">
<?php echo $form->create('Worker');?>
	<fieldset>
 		<legend><?php __('Add Worker');?></legend>
	<?php
		echo $form->input('factory_id');
		echo $form->input('application_id');
		echo $form->input('version');
		echo $form->input('development', array('label' => __('This is an unstable development version, beta or release candidate', true)));
		echo $form->input('Format');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Workers', true), array('action'=>'index'));?></li>
		<li><?php echo $html->link(__('List Factories', true), array('controller'=> 'factories', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Factory', true), array('controller'=> 'factories', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Applications', true), array('controller'=> 'applications', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Application', true), array('controller'=> 'applications', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Formats', true), array('controller'=> 'formats', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Format', true), array('controller'=> 'formats', 'action'=>'add')); ?> </li>
	</ul>
</div>
