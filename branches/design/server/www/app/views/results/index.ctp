<div class="results index">
<h2><?php __('Results');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('factory_id');?></th>
	<th><?php echo $paginator->sort('format_id');?></th>
	<th><?php echo $paginator->sort('filename');?></th>
	<th><?php echo $paginator->sort('path');?></th>
	<th><?php echo $paginator->sort('mimetype_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('updated');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($results as $result):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $result['Result']['id']; ?>
		</td>
		<td>
			<?php echo $html->link($result['Factory']['name'], array('controller'=> 'factories', 'action'=>'view', $result['Factory']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($result['Format']['name'], array('controller'=> 'formats', 'action'=>'view', $result['Format']['id'])); ?>
		</td>
		<td>
			<?php echo $result['Result']['filename']; ?>
		</td>
		<td>
			<?php echo $result['Result']['path']; ?>
		</td>
		<td>
			<?php echo $html->link($result['Mimetype']['name'], array('controller'=> 'mimetypes', 'action'=>'view', $result['Mimetype']['id'])); ?>
		</td>
		<td>
			<?php echo $result['Result']['created']; ?>
		</td>
		<td>
			<?php echo $result['Result']['updated']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action'=>'view', $result['Result']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $result['Result']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $result['Result']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $result['Result']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Result', true), array('action'=>'add')); ?></li>
		<li><?php echo $html->link(__('List Factories', true), array('controller'=> 'factories', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Factory', true), array('controller'=> 'factories', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Formats', true), array('controller'=> 'formats', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Format', true), array('controller'=> 'formats', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Mimetypes', true), array('controller'=> 'mimetypes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Mimetype', true), array('controller'=> 'mimetypes', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
	</ul>
</div>
