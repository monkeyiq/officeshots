<div class="results view">
<h2><?php echo $result['Job']['Application']['name'];?> <?php echo $result['Job']['version'];?> (<?php echo $result['Job']['Platform']['name'];?>)</h2>
<span><?php printf(__('For %s', true), $html->link(
		$result['Job']['Request']['filename'],
		array(
			'controller' => 'requests',
			'action' => 'view',
			$result['Job']['Request']['id']
		)
));?></span><br /><br />
	<?php if ($result['Result']['state'] == Result::STATE_SCAN_FOUND): ?>
		<img src="/img/icons/virus.png" alt="" style="float: left;" />
	<?php else: ?>
		<img src="/img/icons/<?php echo $result['Mimetype']['icon'];?>" alt="" style="float: left;" />
	<?php endif; ?>
	<dl style="margin-left: 10em;"><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Filename'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Result']['filename']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Format'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Format']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $resultModel->getState($result); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Requested'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Job']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Uploaded'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Result']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Actions'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php
				if ($result['Result']['state'] == Result::STATE_SCAN_FOUND) {
					echo $html->link(
						__('Download', true),
						array('action' => 'download', $result['Result']['id']),
						array(),
						sprintf(__('This document contains the "%s" virus. Are you sure?', true), $result['Result']['state_info'])
					);
				} else {
					echo $html->link(__('Download', true), array('action' => 'download', $result['Result']['id']));
				}
			?>
			<?php if ($canDelete == true) {
				echo ' - ' . $html->link(__('Delete', true), array('action' => 'delete', $result['Result']['id']));
			}?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="related">
	<h3><?php printf(__('Rendered by %s', true), $result['Factory']['User']['name']);?></h3>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Factory'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($result['Factory']['name'], array('controller' => 'factories', 'action' => 'view', $result['Factory']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Application'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Job']['Application']['name'];?> <?php echo $result['Job']['version'];?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Operating system'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Factory']['Operatingsystem']['name']; ?>
			<?php echo $result['Factory']['Operatingsystem']['version']; ?>
			(<?php echo $result['Factory']['Operatingsystem']['codename']; ?>)
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Hardware'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $result['Factory']['hardware']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
