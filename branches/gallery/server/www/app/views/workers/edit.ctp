<div class="workers form">
<?php echo $form->create('Worker');?>
	<fieldset>
		<legend>
			<?php 
				if ($this->action == 'add') {
					printf(__('Add an application to factory "%s"', true), $this->data['Factory']['name']);
				} else {
					printf(__('Edit this application on factory "%s"', true), $this->data['Factory']['name']);
				}
			?>
		</legend>
	<?php
		if ($this->action == 'add') {
			echo $form->hidden('factory_id');
		}
		echo $form->hidden('Factory.name');
		echo $form->input('application_id');
		echo $form->input('version');
		echo $form->input('development', array('label' => __('This is an unstable development version, beta or release candidate', true)));
		echo $form->input('Format', array('label' => __('Supported output formats', true), 'type' => 'select', 'multiple' => 'checkbox'));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>

</div>
