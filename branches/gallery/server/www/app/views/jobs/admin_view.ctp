<div class="jobs view">
<h2><?php  __('Job');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $job['Job']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Request'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($job['Request']['filename'], array('controller'=> 'requests', 'action'=>'view', $job['Request']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Platform'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($job['Platform']['name'], array('controller'=> 'platforms', 'action'=>'view', $job['Platform']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Application'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($job['Application']['name'], array('controller'=> 'applications', 'action'=>'view', $job['Application']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Version'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $job['Job']['version']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Result'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($job['Result']['filename'], array('controller'=> 'results', 'action'=>'view', $job['Result']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Factory'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($job['Factory']['name'], array('controller'=> 'factories', 'action'=>'view', $job['Factory']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Locked'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $job['Job']['locked']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $job['Job']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Updated'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $job['Job']['updated']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Job', true), array('action'=>'edit', $job['Job']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Job', true), array('action'=>'delete', $job['Job']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['Job']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Requests', true), array('controller'=> 'requests', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Request', true), array('controller'=> 'requests', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Applications', true), array('controller'=> 'applications', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Application', true), array('controller'=> 'applications', 'action'=>'add')); ?> </li>
	</ul>
</div>

<div class="related">
	<h3><?php __('Related Results');?></h3>
	<?php if (!empty($job['Result'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Factory Id'); ?></th>
		<th><?php __('Filename'); ?></th>
		<th><?php __('Path'); ?></th>
		<th><?php __('Mimetype'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
		<tr>
			<td><?php echo $job['Result']['id'];?></td>
			<td><?php echo $job['Result']['factory_id'];?></td>
			<td><?php echo $job['Result']['filename'];?></td>
			<td><?php echo $job['Result']['path'];?></td>
			<td><?php echo $job['Result']['mimetype'];?></td>
			<td><?php echo $job['Result']['created'];?></td>
			<td><?php echo $job['Result']['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'results', 'action'=>'view', $job['Result']['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'results', 'action'=>'edit', $job['Result']['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'results', 'action'=>'delete', $job['Result']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['Result']['id'])); ?>
			</td>
		</tr>
	</table>
<?php endif; ?>
