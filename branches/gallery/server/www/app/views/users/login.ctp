<p><?php if (Configure::read('Auth.allowRegister')) {
	__('If you do not have an account yet, you can <a href="/users/register">register here</a>. If you have lost your password then you can use our <a href="/users/recover">password recovery</a> form.');
} ?></p>

<?php
	if ($session->check('Message.flash')) {
		$session->flash();
	}
	if ($session->check('Message.auth')) {
		$session->flash('auth');
	}
?>

<p><?php echo sprintf(__('Note: You can also <a href="%s">login using your SSL client certificate</a>', true), 'https://' . env('SERVER_NAME')); ?></p>

<?php echo $form->create('User', array('action' => 'login')); ?>
<fieldset>
	<legend><?php echo __('Login'); ?></legend>
	<?php
		echo $form->input('email_address', array('between' => '<br>', 'class' => 'text'));
		echo $form->input('password', array('between' => '<br>', 'class' => 'text'));
	?>
</fieldset>
<?php echo $form->end('Sign In'); ?>
