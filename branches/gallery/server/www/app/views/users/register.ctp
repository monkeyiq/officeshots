<div class="users form">
<?php echo $form->create('User', array('action' => 'register'));?>
	<fieldset>
 		<legend><?php __('Register a new account');?></legend>
	<?php
		if (Configure::read('Auth.limitRegister')) {
			__('We are in closed beta. Only members of the <a href="http://www.opendocsociety.org/">OpenDoc Society</a> can register. If you wish to join the closed Officeshots beta, please <a href="http://www.opendocsociety.org/join">become an OpenDoc member</a> first.');
		}
		echo $form->input('name');
		echo $form->input('email_address');
		echo $form->input('password', array('value' => ''));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
