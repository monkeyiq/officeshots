<div class="requests view">
<h2><?php  __('Request');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($request['User']['email_address'], array('controller'=> 'users', 'action'=>'view', $request['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('IP address'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo inet_dtop($request['Request']['ip_address']); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Format'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($request['Format']['name'], array('controller'=> 'formats', 'action'=>'view', $request['Format']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Filename'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['filename']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Path'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['path']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Mimetype'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $html->link($request['Mimetype']['name'], array('controller'=> 'mimetypes', 'action'=>'view', $request['Mimetype']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Request', true), array('action'=>'edit', $request['Request']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Request', true), array('action'=>'delete', $request['Request']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $request['Request']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Requests', true), array('action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Request', true), array('action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller'=> 'users', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller'=> 'users', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Doctypes', true), array('controller'=> 'doctypes', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Doctype', true), array('controller'=> 'doctypes', 'action'=>'add')); ?> </li>
		<li><?php echo $html->link(__('List Jobs', true), array('controller'=> 'jobs', 'action'=>'index')); ?> </li>
		<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Jobs');?></h3>
	<?php if (!empty($request['Job'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Request Id'); ?></th>
		<th><?php __('Application Id'); ?></th>
		<th><?php __('Version'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Updated'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($request['Job'] as $job):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $job['id'];?></td>
			<td><?php echo $job['request_id'];?></td>
			<td><?php echo $job['application_id'];?></td>
			<td><?php echo $job['version'];?></td>
			<td><?php echo $job['created'];?></td>
			<td><?php echo $job['updated'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller'=> 'jobs', 'action'=>'view', $job['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller'=> 'jobs', 'action'=>'edit', $job['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller'=> 'jobs', 'action'=>'delete', $job['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Job', true), array('controller'=> 'jobs', 'action'=>'add'));?> </li>
		</ul>
	</div>
</div>
