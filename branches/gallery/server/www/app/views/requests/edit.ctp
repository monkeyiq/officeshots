<?php
	$html->css('wmd', null, array(), false);
	$javascript->link('showdown', false);
	$javascript->link('wmd', false);
?>

<div class="requests view">
<h2><?php printf(__('Filename "%s"', true), $request['Request']['filename']);?></h2>
	<?php if ($request['Request']['state'] == Request::STATE_SCAN_FOUND): ?>
		<img src="/img/icons/virus.png" alt="" style="float: left;" />
	<?php else: ?>
		<img src="/img/icons/<?php echo $request['Mimetype']['icon'];?>" alt="" style="float: left;" />
	<?php endif; ?>
	<dl style="margin-left: 10em;"><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Uploaded'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Request']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $requestModel->getState($request); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Document type'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $request['Mimetype']['Doctype']['name']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<form method="post" action="/requests/edit/<?php echo $request['Request']['id'];?>">
	<h3><?php __('Description');?></h3>
	<div id="wmd-button-bar"></div>
	<textarea id="wmd-input" name="data[Request][description]"><?php echo h($request['Request']['description']);?></textarea>
	<div id="wmd-preview" class="polaroid"></div>
<?php echo $form->end(__('Save', true));?>
	
