<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

App::import('Core', 'BeanStalk.BeanStalkManager');

/**
 * An autonomous worker process to execute deferred model calls
 */
class WorkerShell extends Shell
{
	/** @var object Reference to the BeanStalk queue */
	private $beanstalk = null;

	/**
	 * Print a welcome message
	 */
	public function initialize() {
		$this->out('Cake Autonomous Worker.');
		$this->hr();
	}

	/**
	 * Override startup to parse arguments
	 */
	public function startup()
	{
		$this->tubes = array(BeanStalkManager::$config['default_tube']);
		if (!empty($this->params['tubes'])) {
			$this->tubes = explode(',', $this->params['tubes']);
		}
	}

	/**
	 * Main function. Print help and exit.
	 */
	public function main()
	{
		$this->help();
		$this->_stop();
	}

	public function run()
	{
		$this->out('Connecting to ' . implode(', ', BeanStalkManager::$config['servers']));
		$this->beanstalk =& BeanStalkManager::getBeanStalk();
		$this->out('Connected.');
		
		foreach ($this->tubes as $tube) {
			$tube = trim($tube);
			$this->beanstalk->watch($tube);
			$this->out("Watching tube '$tube'");
		}

		while (true) {
			$job = $this->beanstalk->reserve();

			$message = sprintf('Got job ID %s from beanstalkd', $job->get_jid());

                        $deferred = unserialize($job->get());
			$this->log($message, LOG_DEBUG);
			$this->out($message);

			App::import('Model', $deferred['Model']);
			$model = new $deferred['Model']();
			$model->create();
			$model->id = $deferred['id'];
			$model->data = $deferred['data'];
			call_user_func_array(array($model, $deferred['method']), $deferred['args']);

			$message = sprintf('Processed %s->%s with ID %s', $deferred['Model'], $deferred['method'], $deferred['id']);
			$this->log($message, LOG_DEBUG);
			$this->out($message);
			
			unset($model);
			$job->delete();
		}
	}

	/**
	 * Print help and exit
	 */
	public function help()
	{
		$this->out('Cake Autonomous Worker.');
		$this->hr();
		$this->out("Usage: cake worker <command> <arg1>...");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\trun\n\t\tRun the worker");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('Params:');
		$this->out("\n\ttubes\n\t\tA comma separated list of BeanStalk tubes to watch");
		$this->out('');
	}
}

?>
