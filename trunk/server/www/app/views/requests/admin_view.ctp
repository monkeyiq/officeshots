<div class="requests view">
<h2><?php printf(__('Filename "%s"', true), $request['Request']['filename']);?></h2>
	<?php if ($request['Request']['state'] == Request::STATE_SCAN_FOUND): ?>
		<img src="/img/icons/virus.png" alt="" style="float: left;" />
	<?php else: ?>
		<img src="/img/icons/<?php echo $request['Mimetype']['icon'];?>" alt="" style="float: left;" />
	<?php endif; ?>
	<dl style="margin-left: 10em;">
		<dt><?php __('Id'); ?></dt>
		<dd>
			<?php echo $request['Request']['id']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Path'); ?></dt>
		<dd>
			<?php echo $request['Request']['path'] . '/' . $request['Request']['filename'];?>
			&nbsp;
		</dd>
		<dt><?php __('Uploaded'); ?></dt>
		<dd>
			<?php echo $request['Request']['created'];?> by
			<?php echo $html->link($request['User']['name'], array('controller' => 'users', 'action' => 'view', $request['User']['id']));?> from IP address
			<?php echo inet_dtop($request['Request']['ip_address']);?>
			&nbsp;
		</dd>
		<dt><?php __('Status'); ?></dt>
		<dd>
			<?php echo $requestModel->getState($request); ?>
			&nbsp;
		</dd>
		<dt><?php __('Document type'); ?></dt>
		<dd>
			<?php echo $request['Mimetype']['Doctype']['name']; ?>
			&nbsp;
		</dd>
		<?php if ($request['Validator']): ?>
			<dt><?php __('ODF Validators'); ?></dt>
			<dd>
				<dl>
					<?php foreach ($request['Validator'] as $validator): ?>
						<dt><?php echo $validator['name']; ?></dt>
						<dd><?php echo $validatorModel->getState($validator); ?></dd>
					<?php endforeach; ?>
				</dl>
			</dd>
		<?php endif; ?>
		<dt><?php __('Actions'); ?></dt>
		<dd>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $request['Request']['id']));?> - 
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $request['Request']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $request['Request']['id'])); ?>
		</dd>
	</dl>
</div>

<?php
	if (!empty($request['Request']['description_html'])) {
		echo '<h3>' . __('Description', true) . '</h3>';
		echo $request['Request']['description_html'];
	}
?>

<?php if (!empty($request['Job'])):?>
<div class="related" id="results-list">
	<h3><?php __('Results');?></h3>
	<table>
		<tr>
			<th><?php __('Application');?></th>
			<th><?php __('State');?></th>
			<th><?php __('Validators');?></th>
			<th><?php __('Actions');?></th>
		</tr>
		<?php foreach ($request['Job'] as $job): ?>
			<?php
				$class = 'white';
				if ($job['Result']) {
					switch($job['Result']['verified']) {
						case Result::VERIFY_PASS: $class = 'green'; break;
						case Result::VERIFY_FAIL: $class = 'red'; break;
					}
				}
				echo '<tr class="' . $class . '">';
			?>
				<td class="application">
					<?php
						echo $jobModel->getFormatIcon($job);
						$app = $job['Application']['name'] . ' ' . $job['version']
						    .= ' (' . $job['Platform']['name'] . ')';
						if (empty($job['Result'])) {
							echo $app;
						} else {
							echo $html->link($app, array(
								'controller' => 'results',
								'action' => 'view',
								$job['Result']['id'],
							));
						}
					?>
				</td>
				<td>
					<?php echo $jobModel->getState($job); ?><br />
				</td>
				<td>
				<?php if (isset($job['Result']['Validator'])): ?>
					<?php foreach ($job['Result']['Validator'] as $validator) {
						echo $validatorModel->getStateIcon($validator) . ' ';
					} ?>
				<?php endif;?>
				</td>
				<td>
					<?php
						echo $html->link(__('Delete', true), array(
							'controller' => 'jobs',
							'action' => 'delete',
							$job['id'],
							null,
							sprintf(__('Are you sure you want to delete the job for %s?', true), $app)
						));
					?>
				</td>
			</tr>
		<?php endforeach;?>
	</table>
</div>
<?php endif;?>
