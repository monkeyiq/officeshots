<h2><?php echo $name; ?></h2>
<p class="error">
	<strong><?php __('Error'); ?>: </strong>
	<?php echo sprintf(__("You are not authorised to view %s.", true), "<strong>'{$message}'</strong>")?>
</p>
