<h2>XMLRPC API documentation</h2>

<p>This is the API documentation for the Office-shots XMLRPC interface. It is automatically generated from the source code of the server
using reflection, so it should always be up-to-date.</p>

<h3>Overview</h3>

<p>The Office-shots system consist of a central server and a large number of distributed document factories. Users upload document requests to
the central server and indicate on what office suites and with what settings they want their document to be rendered. The central server then
takes this request, splits it into multiple jobs and puts those jobs in a queue.</p>

<p>The document factories poll the central server and ask for any jobs that match their configuration. When a matching job is found, the job
is locked on the server and the document is sent to the document factory. The factory processes it and uploads the result back to the
central server, which displays the result to the user.</p>

<p>Currently the API only provides for communication between the central server and the document factories. There is no API (yet) that allows
users to programmatically submit requests and retrieve results.</p>

<h3>XMLRPC endpoint</h3>

<p>The XMLRPC endpoint is <a href="https://<?php echo $_SERVER['SERVER_NAME'] ?>/xmlrpc">https://<?php echo $_SERVER['SERVER_NAME'] ?>/xmlrpc</a>.</p>

<h3>XMLRPC function index</h3>

<ul>
<?php foreach($methods as $methodName): ?>
<li><a href="/xmlrpc/<?php echo $methodName; ?>"><?php echo $methodName; ?></a></li>
<?php endforeach; ?>
</ul>

<h3>Authentication</h3>

<p>Authentication is not part of the XMLRPC interface. The XMLRPC endpoint runs on an SSLv3/TLS webserver that requires client certificates
for authentication. In order to use the API you will need an SSLv3/TLS client certificate signed by a trusted Certificate Authority that
verifies your e-mail address. You can obtain such certificates from various Certificate Authorities such as
<a href="http://www.cacert.org/">CACert</a>. The e-mail address on the client certificate must match the account that you registered with
Office-shots. Furthermore, your account must have been granted the privilege to run a factory.</p>

<h3>Factory registration</h3>

<p>Before you can use the API you will need to register your factory with the Office-shots server. A factory corresponds to a single (virtual) machine
that you are running. When you have registered your factory you need to add workers to it. A worker is an instance of an office suite application
that runs of your factory. For every worker you need to set the version number and which output formats it supports.</p>

<p>An example. If you have a Linux machine and a Windows machine you would register two factories. Each factory needs to have a different name. These names
do not need to be globally unique but they must be unique withing your Account. That means you can have only one factory called "Ubuntu box" but it does
not matter if other people also have a factory called "Ubuntu box". You would add all the office suite applications that your Linux machine runs to that
"Ubuntu box" factory, such as OpenOffice.org Writer, Calc and Impress (all version 3.0), AbiWord and Zoho Office (using Konqueror). To the
Windows factory you would add e.g. MS-Word 2007 SP2 and Google Docs (using Internet Explorer 8 beta).</p>

<p>Once you have registered your factories you can start polling for jobs by using their name.</p>

<h3>Notes on the development server</h3>

<p>The development server has a few additional restrictions. The development server uses mod_gnutls instead of mod_ssl to add SSLv3/TLS support
to Apache because there are multiple SSL hosts on the development server. This means that the development server has the following additional
restrictions:</p>

<ul>
<li><p>Your XMLRPC client must support <a href="http://en.wikipedia.org/wiki/Server_Name_Indication">Server Name Indication (SNI)</a>. Without
SNI it is not possible for mod_gnutls to determine which virtual host your request is for. It will not be able to present the correct server
certificate, nor be able to authenticate your client certificate.</p>

<p>The development server API has been succesfully tested with Python's built-in libxmlrpc client using Python's httplib.HTTPS connection
as the transport. Note that httplib.HTTPS cannot verify server certificates. It should also be possible to use Python's M2Crypto XMLRPC wrappers
but these have not been tested.</p></li>

<li><p>The development server runs Debian Lenny. There is <a href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=511573">a bug in Debian's mod_gnutls</a> that prevents it from loading
all of the standard Certificate Authorities that Debian ships. The bug has been fixed upstream but still needs to be backported to Debian Lenny.
Until it is backported, the development server will only accept client certificates signed by <a href="http://www.cacert.org/">CACert</a>.</li>
</ul>

<p>Note that the production version of Office-shots has no such restrictions because it runs mod_ssl instead of mod_gnutls</a>.</p>

<p>Also, to aid in development of factories, the requests on the development server never expire and the job lock timeout is set to zero instead
of five minutes. This ensures that there are always jobs to be processed by factories who use the development server API.</p>
