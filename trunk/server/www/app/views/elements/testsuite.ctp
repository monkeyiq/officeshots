<?php foreach ($gallery['children'] as $child):?>
	<tr class="subgallery">
		<td class="subgallery-name" style="padding-left: <?php echo $indent;?>em"><?php echo $html->link($child['Gallery']['name'] . '/', array('action' => 'view', $child['Gallery']['slug']));?></td>
		<td>&nbsp;</td>
		<?php for ($i = 0; $i < count($applications); $i++): ?>
			<td>-</td>
		<?php endfor; ?>
		<td>-</td>
		<?php if ($access):?><td>&nbsp;</td><?php endif;?>
	</tr>
	<?php echo $this->element('testsuite', array('gallery' => $child, 'indent' => $indent + 2)); ?>
<?php endforeach;?>

<?php if (isset($gallery['Request']) && is_array($gallery['Request'])):?>
<?php foreach ($gallery['Request'] as $request):?>
	<tr class="testsuite-request">
		<td class="request-name" style="padding-left: <?php echo $indent;?>em">
			<?php echo $html->link($request['filename'], array('controller'=> 'requests', 'action'=>'view', $request['id'])); ?>
		</td>
		<td><?php foreach ($request['Validator'] as $validator) { echo $validatorModel->getStateIcon($validator) . ' '; } ?></td>
		<?php foreach ($applications as $application):?>
			<?php
				foreach ($request['Job'] as $job) {
					$match = (
						$job['Application']['name'] == $application['Application']['name']
						&& $job['version'] == $application['Application']['version']
						&& $job['Format']['code'] == 'odf'
					);

					if ($match) {
						$class = 'white';
						if ($job['Result']) {
							switch($job['Result']['verified']) {
								case Result::VERIFY_PASS: $class = 'green'; break;
								case Result::VERIFY_FAIL: $class = 'red'; break;
							}
						}
						echo '<td class="' . $class . '">';

						if (!$job['Result'] || !$job['Result']['id']) {
							echo $html->image('/img/icons/validator-pending.png');
							break;
						}

						if (isset($job['Result']['Validator'])) {
							foreach ($job['Result']['Validator'] as $validator) {
								echo $validatorModel->getStateIcon($validator) . ' ';
							}
						}

						$request['result_count']--;
						break;
					}
				}

				if (!$match) {
					echo '<td> -';
				}
			?></td>
		<?php endforeach;?>
		<td>
			<?php
				if ($request['result_count']) {
					 echo $html->link(
						str_replace(' ', '&nbsp;', sprintf(__('%d more', true), $request['result_count'])),
						array('controller'=> 'requests', 'action'=>'view', $request['id']),
						array('escape' => false)
					);
				}
			?>
		</td>
		<?php if ($access):?>
			<td class="actions">
				<?php echo $html->link(__('Remove', true), array('action' => 'remove_document', $gallery['Gallery']['slug'], $request['id'])); ?>
			</td>
		<?php endif;?>
	</tr>
<?php endforeach; ?>
<?php endif; ?>
