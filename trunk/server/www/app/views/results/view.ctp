<div class="results view">
<h2><?php echo $result['Job']['Application']['name'];?> <?php echo $result['Job']['version'];?> (<?php echo $result['Job']['Platform']['name'];?>)</h2>
<span><?php printf(__('For %s', true), $html->link(
		$result['Job']['Request']['filename'],
		array(
			'controller' => 'requests',
			'action' => 'view',
			$result['Job']['Request']['id']
		)
));?></span><br /><br />

	<?php if ($result['Result']['state'] == Result::STATE_SCAN_FOUND): ?>
		<img src="/img/icons/virus.png" alt="" style="float: left;" />
	<?php else: ?>
		<img src="/img/icons/<?php echo $result['Mimetype']['icon'];?>" alt="" style="float: left;" />
	<?php endif; ?>
	<dl style="margin-left: 10em;">
		<dt><?php __('Filename'); ?></dt>
		<dd>
			<?php echo $result['Result']['filename']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Application'); ?></dt>
		<dd>
			<?php echo $result['Job']['Application']['name'];?> <?php echo $result['Job']['version'];?>
			(<?php echo $result['Job']['Platform']['name'];?>)
			&nbsp;
		</dd>
		<dt><?php __('Format'); ?></dt>
		<dd>
			<?php echo $result['Format']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Status'); ?></dt>
		<dd>
			<?php echo $resultModel->getState($result); ?>
			&nbsp;
		</dd>
		<dt><?php __('Manual verification'); ?></dt>
		<dd>
			<?php echo $resultModel->getVerified($result); ?>
			&nbsp;
		</dd>
		<dt><?php __('Requested'); ?></dt>
		<dd>
			<?php echo $result['Job']['created']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Uploaded'); ?></dt>
		<dd>
			<?php echo $result['Result']['created']; ?>
			&nbsp;
		</dd>
		<?php if ($result['Validator']): ?>
			<dt><?php __('ODF Validators'); ?></dt>
			<dd>
				<dl>
					<?php foreach ($result['Validator'] as $validator): ?>
						<dt><?php echo $validator['name']; ?></dt>
						<dd><?php echo $validatorModel->getState($validator); ?></dd>
					<?php endforeach; ?>
				</dl>
			</dd>
		<?php endif; ?>
		<dt><?php __('Actions'); ?></dt>
		<dd>
			<?php
				if ($this->action == 'admin_view') {
					echo $html->link(__('Delete', true), array('controller' => 'jobs', 'action' => 'delete', $result['Job']['id']));
				} else {
					if ($result['Result']['state'] == Result::STATE_SCAN_FOUND) {
						echo $html->link(
							__('Download', true),
							array('action' => 'download', $result['Result']['id']),
							array(),
							sprintf(__('This document contains the "%s" virus. Are you sure?', true), $result['Result']['state_info'])
						);
					} else {
						echo $html->link(__('Download', true), array('action' => 'download', $result['Result']['id']));
					}

					if ($writeAccess) {
						if ($result['Factory']['id']) {
							echo ' - ' . $html->link(__('Edit', true), array('controller' => 'results', 'action' => 'edit', $result['Result']['id']));
						} else {
							echo ' - ' . $html->link(__('Edit', true), array('controller' => 'jobs', 'action' => 'edit', $result['Job']['id']));
						}
						echo ' - ' . $html->link(__('Delete', true), array('controller' => 'jobs', 'action' => 'delete', $result['Job']['id']));
					}
				}
			?>
			&nbsp;
		</dd>
	</dl>
</div>

<?php
	if (!empty($result['Result']['description_html'])) {
		echo '<h3>' . __('Description', true) . '</h3>';
		echo $result['Result']['description_html'];
	}
?>

<?php if ($result['Factory']['id']):?>
<div class="related">
	<h3><?php printf(__('Rendered by %s', true), $result['Factory']['User']['name']);?></h3>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt><?php __('Factory'); ?></dt>
		<dd>
			<?php echo $html->link($result['Factory']['name'], array('controller' => 'factories', 'action' => 'view', $result['Factory']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php __('Application'); ?></dt>
		<dd>
			<?php echo $result['Job']['Application']['name'];?> <?php echo $result['Job']['version'];?>
			&nbsp;
		</dd>
		<dt><?php __('Operating system'); ?></dt>
		<dd>
			<?php echo $result['Factory']['Operatingsystem']['name']; ?>
			<?php echo $result['Factory']['Operatingsystem']['version']; ?>
			(<?php echo $result['Factory']['Operatingsystem']['codename']; ?>)
			&nbsp;
		</dd>
		<dt><?php __('Hardware'); ?></dt>
		<dd>
			<?php echo $result['Factory']['hardware']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php endif;?>
