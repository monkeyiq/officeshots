<h2><?php __('Add a document');?></h2>

<?php
	echo $form->create('Gallery', array('url' => '/galleries/add_document/' . $slug));
	echo $form->input('requests', array('multiple' => false, 'label' => __('Document', true)));
	echo $form->end(__('Submit', true));
?>
