<div class="mimetypes form">
<?php echo $form->create('Mimetype');?>
	<fieldset>
		<legend><?php
		if ($this->action == 'admin_edit') {
			__('Edit Mimetype');
		} else {
			__('Add Mimetype');
		}
		?></legend>
	<?php
		if ($this->action == 'admin_edit') {
			echo $form->input('id');
		}
		echo $form->input('name');
		echo $form->input('icon', array('type' => 'select', 'options' => $icons, 'empty' => true));
		echo $form->input('extension');
		echo $form->input('doctype_id', array('empty' => true));
		echo $form->input('format_id', array('empty' => true));
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
