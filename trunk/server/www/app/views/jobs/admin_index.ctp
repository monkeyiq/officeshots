<div class="jobs index">
<h2><?php __('Jobs');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('request_id');?></th>
	<th><?php echo $paginator->sort('application_id');?></th>
	<th><?php echo $paginator->sort('platform_id');?></th>
	<th><?php __('State');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($jobs as $job):?>
	<tr>
		<td class="nowrap">
			<?php
				if (empty($job['Result']['id'])) {
					echo $job['Job']['id'];
				} else {
					echo $html->link($job['Job']['id'], array('controller' => 'results', 'action'=>'view', $job['Result']['id']));
				}
			?>
		</td>
		<td>
			<?php echo $html->link($job['Request']['filename'], array('controller'=> 'requests', 'action'=>'view', $job['Request']['id'])); ?>
		</td>
		<td>
			<?php
				$application = $job['Application']['name'] . ' ' . $job['Job']['version'];
				echo $html->link($application, array('controller'=> 'applications', 'action'=>'view', $job['Application']['id']));
			?>
		</td>
		<td>
			<?php echo $html->link($job['Platform']['name'], array('controller'=> 'platforms', 'action'=>'view', $job['Platform']['id'])); ?>
		</td>
		<td>
			<?php echo $jobModel->getState($job); ?><br />
		</td>
		<td class="actions">
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $job['Job']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $job['Job']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->first('<< '.__('first', true).'&nbsp;&nbsp;', array('escape' => false));?>
	<?php echo $paginator->prev('< '.__('previous', true).'&nbsp;&nbsp;', array('escape' => false), null, array('class'=>'disabled', 'escape' => false));?>
   	<?php echo $paginator->numbers();?>&nbsp;&nbsp;
	<?php echo $paginator->next(__('next', true).' >&nbsp;&nbsp;', array('escape' => false), null, array('class'=>'disabled', 'escape' => false));?>
	<?php echo $paginator->last(__('last', true).' >>', array('escape' => false));?>
</div>
