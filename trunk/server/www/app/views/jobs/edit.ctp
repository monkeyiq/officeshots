<?php
	$html->css('wmd', null, array(), false);
	$javascript->link('showdown', false);
	$javascript->link('wmd', false);
?>

<div class="jobs form">
	<?php echo $form->create('Job', array('type' => 'file'));?>
		<h3>
			<?php if ($this->action == 'add' || $this->action == 'admin_add') {
				__('Add a manual result');
			} else {
				__('Settings for this result');
			}
			?>
		</h3>
		<?php
			if ($this->action == 'edit' || $this->action == 'admin_edit') {
				echo $form->input('id');
			}
			echo $form->input('request_id', array('type' => 'hidden'));
		?>
		<?php if ($this->action == 'add' || $this->action == 'admin_add'):?>
			<div class="input">
				<label><?php __('File');?></label>
				<?php echo $form->file('Result.FileUpload'); ?>
			</div>
		<?php endif;?>
		<?php
			echo $form->input('platform_id');
			echo $form->input('application_id');
			echo $form->input('version');
		?>

		<h3><?php __('Manual verification');?></h3>
		<p><?php __('Have you manually checked if this conversion result is correct?');?></p>
		<div class="input">
			<?php echo $form->radio('Result.verified', $verify, array('legend' => false, 'default' => $this->data['Result']['verified']));?>
		</div>

		<h3><?php __('Description');?></h3>
		<div id="wmd-button-bar"></div>
		<textarea id="wmd-input" name="data[Result][description]"><?php echo h($this->data['Result']['description']);?></textarea>
		<div id="wmd-preview" class="polaroid"></div>
	<?php echo $form->submit();?>
</div>
