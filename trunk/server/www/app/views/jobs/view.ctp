<div class="jobs view">
<h2><?php echo $job['Application']['name'];?> <?php echo $job['Job']['version'];?> (<?php echo $job['Platform']['name'];?>)</h2>
<span><?php printf(__('For %s', true), $html->link(
		$job['Request']['filename'],
		array(
			'controller' => 'requests',
			'action' => 'view',
			$job['Request']['id']
		)
));?></span><br /><br />
	<dl>
		<dt><?php __('Id'); ?></dt>
		<dd>
			<?php echo $job['Job']['id']; ?>
			&nbsp;
		</dd>
		<?php if ($job['Factory']['id']):?>
			<dt><?php __('Factory'); ?></dt>
			<dd>
				<?php echo $html->link($job['Factory']['name'], array('controller'=> 'factories', 'action'=>'view', $job['Factory']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php __('Locked'); ?></dt>
			<dd>
				<?php echo $job['Job']['locked']; ?>
				&nbsp;
			</dd>
		<?php endif;?>
		<dt><?php __('Created'); ?></dt>
		<dd>
			<?php echo $job['Job']['created']; ?>
			&nbsp;
		</dd>
		<dt><?php __('State'); ?></dt>
		<dd>
			<?php echo $jobModel->getState($job); ?>
			&nbsp;
		</dd>
		<dt><?php __('Actions'); ?></dt>
		<dd>
			<?php
				if ($inTestsuite && $job['Job']['state'] == Job::STATE_QUEUED) {
					echo $html->link(__('Set as failed', true), array('action' => 'fail', $job['Job']['id']));
				}
				if ($inTestsuite && $job['Job']['state'] == Job::STATE_FAILED) {
					echo $html->link(__('Re-queue', true), array('action' => 'requeue', $job['Job']['id']));
				}
			?>
		</dd>
	</dl>
</div>
