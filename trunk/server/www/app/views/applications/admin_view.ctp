<div class="applications view">
<h2><?php  __('Application');?></h2>
	<dl>
		<dt><?php __('Name'); ?></dt>
		<dd>
			<?php echo $application['Application']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Icon'); ?></dt>
		<dd>
			<?php
				if ($application['Application']['icon']) {
					echo $html->image('/img/icons/applications/' . $application['Application']['icon']);
				} else {
					echo '-';
				}
			?>
		</dd>
		<dt><?php __('Created'); ?></dt>
		<dd>
			<?php echo $application['Application']['created']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Modified'); ?></dt>
		<dd>
			<?php echo $application['Application']['modified']; ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Application', true), array('action'=>'edit', $application['Application']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Application', true), array('action'=>'delete', $application['Application']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $application['Application']['id'])); ?> </li>
	</ul>
</div>

<div class="related">
	<h3><?php __('Related Doctypes');?></h3>
	<?php if (!empty($application['Doctype'])):?>
		<ul>
			<?php foreach ($application['Doctype'] as $doctype):?>
				<li><?php echo $doctype['name'];?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>
