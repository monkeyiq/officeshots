<div class="applications index">
<h2><?php __('Applications');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('icon');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($applications as $application):?>
	<tr>
		<td>
			<?php echo $html->link($application['Application']['name'], array('action'=>'view', $application['Application']['id'])); ?>
		</td>
		<td>
			<?php
				if ($application['Application']['icon']) {
					echo $html->image('/img/icons/applications/' . $application['Application']['icon']);
				} else {
					echo '-';
				}
			?>
		</td>
		<td>
			<?php echo $application['Application']['created']; ?>
		</td>
		<td>
			<?php echo $application['Application']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $application['Application']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $application['Application']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>

<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>

<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Application', true), array('action'=>'add')); ?></li>
	</ul>
</div>
