<div class="doctypes view">
<h2><?php  __('Doctype');?></h2>
	<dl>
		<dt><?php __('Name'); ?></dt>
		<dd>
			<?php echo $doctype['Doctype']['name']; ?>
			&nbsp;
		</dd>
		<dt><?php __('Code'); ?></dt>
		<dd>
			<?php echo $doctype['Doctype']['code']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit Doctype', true), array('action'=>'edit', $doctype['Doctype']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete Doctype', true), array('action'=>'delete', $doctype['Doctype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $doctype['Doctype']['id'])); ?> </li>
		<li><?php echo $html->link(__('List Doctypes', true), array('action'=>'index')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Mimetypes');?></h3>
	<?php if (!empty($doctype['Mimetype'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Name'); ?></th>
		<th><?php __('Icon'); ?></th>
		<th><?php __('Extension'); ?></th>
	</tr>
	<?php foreach ($doctype['Mimetype'] as $mimetype):?>
		<tr>
			<td><?php echo $html->link($mimetype['name'], array('controller'=> 'mimetypes', 'action'=>'view', $mimetype['id'])); ?></td>
			<td><?php echo $mimetype['icon'];?></td>
			<td><?php echo $mimetype['extension'];?></td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
