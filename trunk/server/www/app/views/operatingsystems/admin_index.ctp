<div class="operatingsystems index">
<h2><?php __('Operatingsystems');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('platform_id');?></th>
	<th><?php echo $paginator->sort('created');?></th>
	<th><?php echo $paginator->sort('modified');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php foreach ($operatingsystems as $operatingsystem):?>
	<tr>
		<td>
		<?php
			$name = $operatingsystem['Operatingsystem']['name'] . ' ' . $operatingsystem['Operatingsystem']['version'];
			if ($operatingsystem['Operatingsystem']['codename']) {
				$name .= ' (' . $operatingsystem['Operatingsystem']['codename'] . ')';
			}

			echo $html->link($name, array('action'=>'view', $operatingsystem['Operatingsystem']['id']));
		?>
		</td>
		<td>
			<?php echo $html->link($operatingsystem['Platform']['name'], array('controller'=> 'platforms', 'action'=>'view', $operatingsystem['Platform']['id'])); ?>
		</td>
		<td>
			<?php echo $operatingsystem['Operatingsystem']['created']; ?>
		</td>
		<td>
			<?php echo $operatingsystem['Operatingsystem']['modified']; ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('Edit', true), array('action'=>'edit', $operatingsystem['Operatingsystem']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action'=>'delete', $operatingsystem['Operatingsystem']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $operatingsystem['Operatingsystem']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class'=>'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Operatingsystem', true), array('action'=>'add')); ?></li>
	</ul>
</div>
