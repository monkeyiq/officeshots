<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// We need to access the Request model statically for it's state constants
App::import('Model', 'Request');

/**
 * The jobs controller
 */
class JobsController extends AppController
{
	/** @var array The components this controller uses */
	public $components = array('AuthCert');
	
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form', 'JobModel');

	/** @var array The models used by this controller */
	public $uses = array('Job', 'Factory', 'Request');

	/**
	 * You can't index jobs. Redirect to requests.
	 * @return void
	 */
	public function index()
	{
		$this->redirect(array('controller' => 'requests', 'action' => 'index'));
	}

	/**
	 * Manually add a job and result to a request
	 */
	public function add($request_id = null)
	{
		$this->helpers[] = 'Javascript';

		if (!empty($this->data)) {
			// Load the request
			$request = $this->Job->Request->find('first', array(
				'conditions' => array('Request.id' => $this->data['Job']['request_id']),
				'recursive' => -1,
			));

			// Create a new job
			$this->Job->create();
			$this->Job->set($this->data);
			$this->Job->save();

			// Load the new job from the database
			$this->Job->contain(array('Application', 'Platform'));
			$job = $this->Job->read();

			// Find out where to store the result
			$path  = $request['Request']['root'] . DS;
			$path .= Inflector::slug($job['Application']['name']) . '_';
			$path .= Inflector::slug($job['Job']['version']) . '_';
			$path .= Inflector::slug($job['Platform']['name']);

			// Create a new result
			$this->Job->Result->create();
			$this->Job->Result->set(array(
				'path' => $path,
				'state' => Result::STATE_UPLOADING,
			));

			// Add the file upload
			$errors = array();
			if (!$this->Job->Result->addUpload($this->data, $errors)) {
				$this->Job->delete();
				$this->Session->setFlash(__('Error uploading:<br />', true) . implode("<br />\n", $errors));
				$this->redirect(array('action'=>'add'));
			}
			
			// Fill in the format_id in the job
			$mimetype = $this->Job->Result->Mimetype->find('first', array(
				'conditions' => array('Mimetype.id' => $this->Job->Result->data['Result']['mimetype_id']),
				'recursive' => -1,
			));
			$this->Job->Result->set('format_id', $mimetype['Mimetype']['format_id']);

			$this->log('Result: ' . print_r($this->Job->Result->data, true), LOG_DEBUG);
			$this->log('Mimetype: ' . print_r($mimetype, true), LOG_DEBUG);

			// Save the result
			$this->Job->Result->save();
			$this->Job->save(array(
				'result_id' => $this->Job->Result->id,
				'format_id' => $mimetype['Mimetype']['format_id'],
			));

			// Finally add the description to the result.
			$this->Job->Result->set($this->data['Result']);
			$this->Job->Result->save();

			// All done!
			$this->redirect(array('controller' => 'requests', 'action' => 'view', $this->data['Job']['request_id']));
		}

		if (empty($this->data)) {
			$this->data = array(
				'Job' => array(
					'request_id' => $request_id,
				),
				'Result' => array(
					'verified' => 0,
					'description' => '',
				),
			);
		}

		$platforms = $this->Job->Platform->find('list');
		$applications = $this->Job->Application->find('list');

		$verify = array(
			Result::VERIFY_PENDING => __('Not manually verified', true),
			Result::VERIFY_PASS    => __('Correct result', true),
			Result::VERIFY_FAIL    => __('Incorrect result', true),
		);

		$this->set(compact('platforms', 'applications', 'verify'));
		$this->render('edit');
	}

	/**
	 * View one of your own jobs
	 *
	 * @param string $id The job ID
	 * @return void
	 */
	public function view($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Job.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'index'));
		}
		
		$this->Job->contain(array(
			'Request',
			'Request.Gallery',
			'Platform',
			'Application',
			'Factory',
			'Result',
		));
		$job = $this->Job->read(null, $id);
		
		$isValid = (
			!empty($job['Request']['Gallery']) ||
			$job['Request']['user_id'] == $this->AuthCert->user('id')
		);

		if (!$isValid) {
			$this->Session->setFlash(__('Invalid Job.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'index'));
		}

		$inTestsuite = $this->Job->Request->inTestsuite($job['Request']['id']);
		$this->set(compact('job', 'inTestsuite'));
	}

	/**
	 * Manually change a job's state
	 */
	private function _setState($state, $id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Job.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'index'));
		}
		
		$this->Job->contain(array('Request'));
		$job = $this->Job->read(null, $id);
		
		if (!$this->Job->Request->inTestsuite($job['Request']['id'])) {
			$this->Session->setFlash(__('Invalid Job.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'index'));
		}

		$this->Job->id = $id;
		$this->Job->saveField('state', $state);
	}

	/**
	 * Manually mark job as failed
	 */
	public function fail($id = null)
	{
		$this->_setState(Job::STATE_FAILED, $id);
		$this->redirect(array('action' => 'view', $id));
	}

	/**
	 * Manually mark job as failed
	 */
	public function requeue($id = null)
	{
		$this->_setState(Job::STATE_QUEUED, $id);
		$this->Job->saveField('failures', 0);
		$this->redirect(array('action' => 'view', $id));
	}

	/**
	 * Edit a job
	 * @param string $id The Job ID
	 */
	public function edit($id = null)
	{
		$this->helpers[] = 'Javascript';

		if (!empty($this->data)) {
			if (!$this->Job->Request->checkAccess($this->AuthCert->user('id'), 'write', $this->data['Job']['request_id'])) {
				$this->Session->setFlash(__('Invalid Job.', true));
				$this->redirect(array('controller' => 'requests', 'action'=>'view', $this->data['Job']['request_id']));
			}

			if ($this->Job->save($this->data)) {
				$this->data['Result']['id'] = $this->Job->field('result_id');
				$this->Job->Result->save($this->data);

				$this->Session->setFlash(__('The Job has been saved', true));
				$this->redirect(array('controller' => 'results', 'action'=>'view', $this->data['Result']['id']));
			} else {
				$this->Session->setFlash(__('The Job could not be saved. Please, try again.', true));
			}
		}

		if (empty($this->data)) {
			$this->Job->id = $id;
			$this->Job->contain('Request', 'Result');
			$this->data = $this->Job->read();
		}

		$platforms = $this->Job->Platform->find('list');
		$applications = $this->Job->Application->find('list');

		$verify = array(
			Result::VERIFY_PENDING => __('Not manually verified', true),
			Result::VERIFY_PASS    => __('Correct result', true),
			Result::VERIFY_FAIL    => __('Incorrect result', true),
		);

		$this->set(compact('platforms', 'applications', 'verify'));
	}

	/**
	 * Delete a job
	 * @param string $id The Job ID
	 */
	public function delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid Job.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'index'));
		}

		$this->Job->id = $id;
		$this->Job->contain('Request');
		$job = $this->Job->read();

		if (!$this->Job->Request->checkAccess($this->AuthCert->user('id'), 'write', $job['Request']['id'])) {
			$this->Session->setFlash(__('Invalid Job.', true));
			$this->redirect(array('controller' => 'requests', 'action'=>'view', $job['Request']['id']));
		}

		$this->Job->delete();
		$this->redirect(array('controller' => 'requests', 'action'=>'view', $job['Request']['id']));
	}

	/**
	 * Search for a given Job ID
	 * This allows factory owners to mark problematic jobs in a test suite as invalid
	 */
	public function search()
	{
		if (!empty($this->data)) {
			$job = $this->Job->find('first', array(
				'conditions' => array('Job.id' => $this->data['Job']['id']),
				'recursive' => -1,
			));

			if ($job) {
				$this->redirect(array('action' => 'view', $job['Job']['id']));
			}
			
			$this->Session->setFlash(__('That Job ID does not exist.', true));
		}
	}

	/**
	 * List all jobs
	 * @return void
	 */
	public function admin_index()
	{
		$this->Job->recursive = 0;
		$this->paginate = array('order' => 'Job.id');
		$this->set('jobs', $this->paginate());
	}

	/**
	 * Delete a job
	 *
	 * @param string $id The job ID
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Job', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Job->del($id)) {
			$this->Session->setFlash(__('Job deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	/**
	 * Find a job that matches any of the applications on a given factory and lock it
	 *
	 * Signature
	 * ~~~~~~~~~
	 * jobs.poll(factory_name)
	 *
	 * Arguments
	 * ~~~~~~~~~
	 * - factory_name (string): The name of the factory that you want to poll for
	 *
	 * Return value
	 * ~~~~~~~~~~~~
	 * When a job is found that matches any of the workers that are running on the
	 * requested factory then an array will be returned containing the following fields:
	 *
	 * - job (string): The GUID of the job
	 * - application (string): The name of the application to run, e.g. "OpenOffice.org Writer"
	 * - version (string): The version string of the application, e.g. "3.0" or "2003 SP2"
	 * - pageStart (int): The first page to render in the output
	 * - pageEnd (int): The last page to render in the output. If this is zero then it should be
	 *                  rendered to the end of the document.
	 * - format (string): 3-letter code specifying the desired output format (pdf, png or odf). If this
	 *                    is empty then the factory can decide.
	 * - filename (string): The original filename of the ODF document
	 * - doctype (string): 3-letter code specifying the input type (odt, ods, odp)
	 * - document (string): Base64-encoded contents of the document
	 *
	 * When no mathcing job has been found then an empty result is returned.
	 *
	 * Job locking
	 * ~~~~~~~~~~~
	 * The matching job is locked for five minutes. This is to make sure that no jobs are processed by two factories at
	 * the same time. If your factory takes longer to process a job, it is possible that somebody else will lock it. In
	 * this case, your upload will fail.
	 *
	 * Example request
	 * ~~~~~~~~~~~~~~~
	 * POST /xmlrpc HTTP/1.0
	 * Host: example.org
	 * Content-Type: text/xml
	 * Content-Length: 193
	 *
	 * <?xml version='1.0'?>
	 * <methodCall>
	 *   <methodName>jobs.finish</methodName>
	 *   <params>
	 *     <param><value><string>My factory</string></value></param>
	 *   </params>
	 * </methodCall>
	 *
	 * Example response
	 * ~~~~~~~~~~~~~~~~
	 * HTTP/1.1 200 OK
	 * Content-Type: application/xml
	 * Content-Length: 10164
	 * Connection: close
	 *
	 * <?xml version="1.0" encoding="iso-8859-1"?>
	 * <methodResponse>
	 *   <params>
	 *     <param>
	 *       <value>
	 *         <struct>
	 *           <member>
	 *             <name>job</name>
	 *             <value><string>4975c9c6-79a0-43a1-8134-0ba5c0a80105</string></value>
	 *           </member>
	 *           <member>
	 *             <name>application</name>
	 *             <value><string>OpenOffice.org Writer</string></value>
	 *           </member>
	 *           <member>
	 *             <name>version</name>
	 *             <value><string>3.0</string></value>
	 *           </member>
	 *           <member>
	 *             <name>pageStart</name>
	 *             <value><string>1</string></value>
	 *           </member>
	 *           <member>
	 *             <name>pageEnd</name>
	 *             <value><string>0</string></value>
	 *           </member>
	 *           <member>
	 *             <name>format</name>
	 *             <value><string>pdf</string></value>
	 *           </member>
	 *           <member>
	 *             <name>filename</name>
	 *             <value><string>mydocument.odt</string></value>
	 *           </member>
	 *           <member>
	 *             <name>doctype</name>
	 *             <value><string>odt</string></value>
	 *           </member>
	 *           <member>
	 *             <name>document</name>
	 *             <value><string>UEsDBBQA ... AAAAAA==</string></value>
	 *           </member>
	 *         </struct>
	 *       </value>
	 *     <param>
	 *   </params>
	 * </methodResponse>
	 *
	 * @param string $method The XMLRPC method name
	 * @param mixed $params The XMLRPC parameters
	 * @param mixed $userdata Data passed from the XMLRPC server
	 * @return array The XMLRPC result or a fault array
	 */
	public function xmlrpc_poll($method, $params, $userdata = null)
	{
		if (!$factory = $this->Factory->findFactory($this->AuthCert->user('id'), $params[0])) {
			return array('faultCode' => 1, 'faultString' => 'Factory does not exist. Possible SSL authentication failure.');
		}

		$this->Factory->id = $factory['Factory']['id'];
		$this->Factory->poll();

		$jobs = array();
		foreach ($factory['Worker'] as $worker) {
			$formats = array_merge(array(''), Set::extract('/Format/id', $worker));
			$doctypes = array_merge(array(''), Set::extract('/Doctype/id', $worker));
			foreach ($formats as &$format) { $format = "'" . $format . "'"; }
			foreach ($doctypes as &$doctype) { $doctype = "'" . $doctype . "'"; }

			$sqlWhere = '';
			if ($worker['testsuite'] == 0) {
				$sqlWhere = 'AND `Request`.`priority` < 10';
			}

			$jobSet = $this->Job->query("SELECT
					`Job`.`id`,
					`Job`.`factory_id`,
					`Job`.`version`,
					`Job`.`locked`,
					`Job`.`failures`,
					`Job`.`state`,
					`Application`.`name`,
					`Request`.`id`,
					`Request`.`page_start`,
					`Request`.`page_end`,
					`Request`.`filename`,
					`Request`.`created`,
					`Request`.`priority`,
					`Request`.`own_factory`,
					`Request`.`path`,
					`Format`.`code`,
					`Doctype`.`code`
				FROM `jobs` as `Job`
					LEFT JOIN `requests` AS `Request` ON (`Job`.`request_id` = `Request`.`id`)
					LEFT JOIN `applications` AS `Application` ON (`Job`.`application_id` = `Application`.`id`)
					LEFT JOIN `formats` AS `Format` ON (`Job`.`format_id` = `Format`.`id`)
					LEFT JOIN `mimetypes` AS `Mimetype` on (`Request`.`mimetype_id` = `Mimetype`.`id`)
					LEFT JOIN `doctypes` AS `Doctype` on (`Mimetype`.`doctype_id` = `Doctype`.`id`)
				WHERE `Job`.`result_id` = ''
					AND `Job`.`state` = " . Job::STATE_QUEUED . "
					AND `Job`.`locked` < '" . date('Y-m-d H:i:s') . "'
					AND `Job`.`platform_id` = '" . $factory['Operatingsystem']['platform_id'] . "'
					AND `Job`.`application_id` = '" . $worker['application_id'] . "'
					AND `Job`.`version` = '" . $worker['version'] . "'
					AND `Job`.`format_id` IN  (" . implode(', ', $formats) . ")
					AND `Request`.`state` = " . Request::STATE_QUEUED . "
					AND `Mimetype`.`doctype_id` IN (" . implode(', ', $doctypes) . ")
					AND (`Request`.`own_factory` = 0
						OR (`Request`.`own_factory` = 1 AND `Request`.`user_id` = '" . $this->AuthCert->user('id') . "')
					)
					$sqlWhere
				ORDER BY `Request`.`own_factory` DESC,
					`Request`.`priority` ASC,
					`Job`.`locked` ASC,
					`Request`.`created` ASC
				LIMIT 0,1");
			$jobs = array_merge($jobs, $jobSet);
		}

		if (sizeof($jobs) == 0) {
			return null;
		}

		usort($jobs, array($this, '_cmpJobs'));
		$job = array_shift($jobs);

		$this->Request->id = $job['Request']['id'];
		$file = $this->Request->getPath();
		if (!file_exists($file) || !is_readable($file)) {
			// Something went wrong. Expire the entire request
			$this->Request->saveField('expire', date('Y-m-d H:i:s', time() -1));
			$this->Request->expire();

			return array('faultCode' => 2, 'faultString' => 'Job document could not be read. Please poll again.');
		}

		// If a factory ID has been set, this job has been handed out earlier. Increase the failure count
		if ($job['Job']['factory_id']) {
			$job['Job']['failures'] += 1;

			// If this job has failed too many times, change it's state and prompt the factory to poll again
			if ($job['Job']['failures'] > Configure::read('Job.maxfailures')) {
				$job['Job']['state'] = Job::STATE_FAILED;
				$this->Job->save($job);
				
				$this->log(sprintf("Job ID %s failed %s times. Stopping job.", $job['Job']['id'], Configure::read('Job.maxfailures')), LOG_DEBUG);
				return null;
			}
		}

		$job['Job']['locked'] = date('Y-m-d H:i:s', time() + Configure::read('Job.locktime'));
		$job['Job']['factory_id'] = $factory['Factory']['id'];
		$this->Job->save($job);

		$result = array(
			'job' => $job['Job']['id'],
			'application' => $job['Application']['name'],
			'version' => $job['Job']['version'],
			'pageStart' => $job['Request']['page_start'],
			'pageEnd' => $job['Request']['page_end'],
			'format' => ($job['Format'] ? $job['Format']['code'] : ''),
			'filename' => $job['Request']['filename'],
			'doctype' => $job['Doctype']['code'],
			'document' => base64_encode(file_get_contents($file))
		);

		return $result;
	}

	/**
	 * A helper function to sort jobs for xmlrpc_poll()
	 */
	public function _cmpJobs($a, $b)
	{
		// First, look at the own_factory flag
		if ($a['Request']['own_factory'] == 1 && $b['Request']['own_factory'] == 0) {
			return -1;
		}
		if ($a['Request']['own_factory'] == 0 && $b['Request']['own_factory'] == 1) {
			return 1;
		}

		// Next, look at the priority
		if ($a['Request']['priority'] < $b['Request']['priority']) {
			return -1;
		}
		if ($a['Request']['priority'] > $b['Request']['priority']) {
			return 1;
		}

		// If the priorities are also equal, look at the locked time
		return ($a['Job']['locked'] < $b['Job']['locked']) ? -1 : 1;
	}

	/**
	 * Finish a job by uploading the result
	 *
	 * Signature
	 * ~~~~~~~~~
	 * jobs.finish(factory_name, job, format, document)
	 * 
	 * Arguments
	 * ~~~~~~~~~
	 * - factory_name (string): The name of the factory that processed the job.
	 * - job (string): The job ID that was returned by jobs.poll().
	 * - format (string): 3-letter code specifying the format of the result (pdf, png or odf).
	 * - document (string): Base64-encoded contents of the result document.
	 *
	 * Return value
	 * ~~~~~~~~~~~~
	 * - result (string): The ID of the result
	 *
	 * Example request
	 * ~~~~~~~~~~~~~~~
	 * POST /xmlrpc HTTP/1.0
	 * Host: example.org
	 * Content-Type: text/xml
	 * Content-Length: 10162
	 *
	 * <?xml version='1.0'?>
	 * <methodCall>
	 *   <methodName>jobs.finish</methodName>
	 *   <params>
	 *     <param><value><string>My factory</string></value></param>
	 *     <param><value><string>4975c9c6-79a0-43a1-8134-0ba5c0a80105</string></value></param>
	 *     <param><value><string>pdf</string></value></param>
	 *     <params><value><string>UEsDBBQA ... AAAAAA==</string></value></params>
	 *   </params>
	 * </methodCall>
	 *
	 * Example response
	 * ~~~~~~~~~~~~~~~~
	 * HTTP/1.1 200 OK
	 * Content-Type: application/xml
	 * Content-Length: 195
	 * Connection: close
	 *
	 * <?xml version="1.0" encoding="iso-8859-1"?>
	 * <methodResponse>
	 *   <params>
	 *     <param><value><string>497d9737-a2cc-4682-9d90-0136c0a80105</string></value></param>
	 *   </params>
	 * </methodResponse>
	 *
	 * @param string $method The XMLRPC method name
	 * @param mixed &$params The XMLRPC parameters
	 * @param mixed $userdata Data passed from the XMLRPC server
	 * @return array The XMLRPC result or a fault array
	 */
	public function xmlrpc_finish($method, &$params, $userdata = null)
	{
		if (sizeof($params) != 4) {
			return array('faultCode' => 1, 'faultString' => 'Wrong parameter count.');
		}
		list($factory_name, $job_id, $format_code) = $params;

		if (!$factory = $this->Factory->findFactory($this->AuthCert->user('id'), $factory_name)) {
			return array('faultCode' => 1, 'faultString' => 'Factory does not exist.');
		}

		// Check the job
		$job = $this->Job->find('first', array(
			'conditions' => array('Job.id' => $job_id),
			'contain' => array(
				'Request',
				'Format',
				'Application',
				'Platform',
			)
		));

		if (!$job) {
			return array('faultCode' => 1, 'faultString' => 'Job does not exist.');
		}

		if ($job['Job']['factory_id'] != $factory['Factory']['id']) {
			return array('faultCode' => 1, 'faultString' => 'Job lock expired and the job is taken by another factory.');
		}

		// Check the return format
		$format = $this->Job->Format->find('first', array(
			'recursive' => -1,
			'conditions' => array('Format.code' => $format_code),
		));

		if (!$format || ($job['Job']['format_id'] && $job['Format']['code'] != $format_code)) {
			return array('faultCode' => 1, 'faultString' => 'Wrong document format.');
		}

		// TODO: The file extension stuff is an ugly kludge. It should do this through the Mimetype model
		$basename = basename($job['Request']['filename']);
		$basename = substr($basename, 0, strrpos($basename, '.'));
		if ($format_code == 'odf') {
			$filename = $basename . strrchr($job['Request']['filename'], '.');
		} else {
			$filename = $basename . '.' . $format_code;
		}

		// Find out where to store the result
		$path  = $job['Request']['root'] . DS;
		$path .= Inflector::slug($job['Application']['name']) . '_';
		$path .= Inflector::slug($job['Job']['version']) . '_';
		$path .= Inflector::slug($job['Platform']['name']);

		// Create the Result
		$this->Job->Result->create();
		$this->Job->Result->set(array(
			'format_id'  => $format['Format']['id'],
			'factory_id' => $factory['Factory']['id'],
			'path' => $path
		));

		if (!$this->Job->Result->setFileBuffer($params[3], $filename, 'convert.base64-decode')) {
			$errors = $this->Job->Result->Behaviors->File->errors;
			array_unshift($errors, 'The result file could not be stored');
			return array('faultCode' => 1, 'faultString' => implode("\n", $errors));
		}

		if (!$this->Job->Result->save()) {
			return array('faultCode' => 1, 'faultString' => 'The result could not be saved');
		}

		// Update the Job with the new Result
		$this->Job->id = $job['Job']['id'];
		$this->Job->save(array(
			'result_id' => $this->Job->Result->id,
			'state' => Job::STATE_FINISHED,
		));

		return $this->Job->Result->id;
	}
}

?>
