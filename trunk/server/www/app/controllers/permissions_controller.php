<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Controller for the permissions
 *
 * This controller only contains admin functions.
 */
class PermissionsController extends AppController
{
	/** @var array The helpers that will be available on the view */
	public $helpers = array('Html', 'Form');

	/** @var array The components this controller uses */
	public $components = array('AuthCert');

	/**
	 * Add a new permission to the specified group
	 *
	 * @param string $group_id The Group to add the permission to
	 * @return void
	 */
	function admin_add($group_id = null)
	{
		if (!$group_id) {
			$this->Session->setFlash(__('Invalid group ID', true));
			$this->redirect(array('controller' => 'groups', 'action'=>'index'));
		}

		if (!empty($this->data)) {
			$this->Permission->create();
			$this->data['Permission']['group_id'] = $group_id;
			if ($this->Permission->save($this->data)) {
				$this->Session->setFlash(__('The Permission has been saved', true));
				$this->redirect(array('controller' => 'groups', 'action'=>'view', $group_id));
			} else {
				$this->Session->setFlash(__('The Permission could not be saved. Please, try again.', true));
			}
		}

		$this->set(compact('group_id'));
	}

	/**
	 * Delete a permission
	 *
	 * @param string $id The permission ID
	 * @return void
	 */
	function admin_delete($id = null)
	{
		if (!$id) {
			$this->Session->setFlash(__('Invalid permission ID', true));
			$this->redirect(array('controller' => 'groups', 'action'=>'index'));
		}

		$this->Permission->id = $id;
		$group_id = $this->Permission->field('group_id');

		if ($this->Permission->del($id)) {
			$this->Session->setFlash(__('Permission deleted', true));
			$this->redirect(array('controller' => 'groups', 'action'=>'view', $group_id));
		}
	}
}

?>
