<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The doctypes controller
 */
class DoctypesController extends AppController {

	var $name = 'Doctypes';
	var $helpers = array('Html', 'Form');

	function admin_index() {
		$this->Doctype->recursive = 0;
		$this->set('doctypes', $this->paginate());
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Doctype.', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->set('doctype', $this->Doctype->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->Doctype->create();
			if ($this->Doctype->save($this->data)) {
				$this->Session->setFlash(__('The Doctype has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Doctype could not be saved. Please, try again.', true));
			}
		}

		$this->render('admin_edit');
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Doctype', true));
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->data)) {
			if ($this->Doctype->save($this->data)) {
				$this->Session->setFlash(__('The Doctype has been saved', true));
				$this->redirect(array('action'=>'index'));
			} else {
				$this->Session->setFlash(__('The Doctype could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Doctype->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Doctype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Doctype->del($id)) {
			$this->Session->setFlash(__('Doctype deleted', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	function admin_reset() {
		if ($this->Doctype->resetweights()) {
			$this->Session->setFlash(__('Doctype order reset', true));
			$this->redirect(array('action'=>'index'));
		}
	}

	function admin_moveup($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Doctype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Doctype->moveup($id)) {
			$this->redirect(array('action'=>'index'));
		}
	}

	function admin_movedown($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Doctype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Doctype->movedown($id)) {
			$this->redirect(array('action'=>'index'));
		}
	}
}
?>
