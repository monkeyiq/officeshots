<?php
/**
 * Officeshots.org - Test your office documents in different applications
 * Copyright (C) 2009 Stichting Lone Wolves
 * Written by Sander Marechal <s.marechal@jejik.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A shell to manage jobs (and results)
 */
class JobsShell extends Shell
{
	/** @var array The models to use */
	public $uses = array('Gallery', 'GalleriesRequest', 'Job', 'Testsuite');

	/**
	 * Main function. Print help and exit.
	 */
	public function main()
	{
		$this->help();
	}

	/**
	 * Re-run a subset or all validators for a test suite
	 */
	public function rerun()
        {
                $jobs = $this->_getJobs();
                foreach ($jobs as $job) {
                        $this->Job->id = $job['Job']['id'];
                        $this->Job->rerun();
                }

		$this->out(sprintf('Re-running %d jobs.', sizeof($jobs)));
        }

        /**
         * Return a list of all selected jobs
         * 
         * @return array
         */
        private function _getJobs()
        {
                // Set all the filter variables
		$suite_id = $this->_getSuite();
		list($application_id, $version) = $this->_getApplication();

                // Get all applicable jobs
                $jobs = $this->Job->find('all', array(
                        'conditions' => array(
                                'Job.application_id' => $application_id,
                                'Job.version' => $version,
                        ),
                        'contain' => array('Request'),
                ));

                // Manually filter on gallery_id
                if ($suite_id) {
                        $this->Testsuite->id = $suite_id;
                        $gallery_id = $this->Testsuite->field('gallery_id');
                        $galleries = Set::extract('/Gallery/id', $this->Gallery->children($gallery_id));
                        $galleries[] = $gallery_id;

                        $count = count($jobs);
                        for($i = 0; $i < $count; $i++) {
                                $inGallery = (bool) $this->GalleriesRequest->find('count', array(
                                        'conditions' => array(
                                                'request_id' => $jobs[$i]['Request']['id'],
                                                'gallery_id' => $galleries,
                                        ),
                                        'recursive' => -1,
                                ));

                                if (!$inGallery) {
                                        unset($jobs[$i]);
                                }
                        }
                }

                return $jobs;
	}

	/**
	 * Ask for a test suite
	 */
	private function _getSuite()
	{
		$options = array(1 => 'All test suites');
		$suites = $this->Testsuite->find('list', array(
			'order' => 'Testsuite.name ASC',
			'recursive' => -1,
		));

		foreach ($suites as $suite) {
			$options[] = $suite;
		}

		$this->out('Please coose a test suite.');
		$this->hr();
		foreach ($options as $index => $option) {
			$this->out(sprintf('[%d] %s', $index, $option));
		}

		$suite_id = null;
		while ($suite_id === null) {
			$suiteNum = $this->in('Enter a test suite number, or q to quit', null, 'q');

			if (strtolower($suiteNum) === 'q') {
				$this->out('Exit');
				$this->_stop();
			}

			$suiteNum = intval($suiteNum) - 2;
			if ($suiteNum >= count($suites)) {
				$this->out('Invalid selection');
				continue;
			}

			if ($suiteNum == -1) {
				$suite_id = false;
			} else {
				$suite_ids = array_keys($suites);
				$suite_id = $suite_ids[$suiteNum];
			}
		}

		return $suite_id;
	}

	/**
	 * Ask what applications to select
	 */
	private function _getApplication()
	{
                $applications = $this->Job->query('SELECT DISTINCT
                                `Application`.`id`,
                                `Application`.`name`,
                                `Job`.`version`
                        FROM `jobs` AS `Job`
                        LEFT JOIN `applications` AS `Application`
                                ON (`Job`.`application_id` = `Application`.`id`)
                        ORDER BY
                                `Application`.`name`,
                                `Job`.`version`
                ');

		$options = array();
                foreach ($applications as $i => $application) {
                        $options[$i + 1] = $application['Application']['name'] . ' ' . $application['Job']['version'];
                }
                        
		$this->out('Please choose an application.');
		$this->hr();
		foreach ($options as $index => $option) {
			$this->out(sprintf('[%d] %s', $index, $option));
		}

		$application = '';
		while (true) {
			$applicationNum = $this->in('Enter a application number, or q to quit', null, 'q');

			if (strtolower($applicationNum) === 'q') {
				$this->out('Exit');
				$this->_stop();
			}

			$applicationNum = intval($applicationNum) - 1;
			if ($applicationNum > count($options)) {
				$this->out('Invalid selection');
				continue;
			}

                        if (isset($applications[$applicationNum])) {
                                return array(
                                        $applications[$applicationNum]['Application']['id'],
                                        $applications[$applicationNum]['Job']['version'],
                                );
                        }
		}
	}

	/**
	 * Print shell help
	 */
	public function help()
	{
		$this->out('Commandline interface to work with the jobs of the test suites');
		$this->hr();
		$this->out("Usage: cake jobs <command>");
		$this->hr();
		$this->out('Commands:');
		$this->out("\n\trerun\n\t\tRe-run a subset of jobs for a test suite.");
		$this->out("\t\tThis will delete existing results and regenerate them.");
		$this->out("\n\thelp\n\t\tShow this help");
		$this->out('');
	}
}

?>
