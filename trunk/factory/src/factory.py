#!/usr/bin/env python
# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2009 Stichting Lone Wolves
# Written by Sander Marechal <s.marechal@jejik.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
This is the core Factory class and main method of the Officeshots
factory. Execute it with -h or --help to see the available options
"""

import os
import sys
import time
import socket
import logging
import platform
import traceback
import base64
import httplib
import ssl
import socket

from optparse import OptionParser
from backends import BackendException
from xml.parsers.expat import ExpatError
from daemon import Daemon

import ConfigParser
from xmlrpclib import ServerProxy, Error, Fault
#from xmlrpc.client import ServerProxy, Error, Fault
#import configparser

scriptPath = ""

LOGLEVELS = {'debug': logging.DEBUG,
             'info': logging.INFO,
	     'warning': logging.WARNING,
	     'error': logging.ERROR,
	     'critical': logging.CRITICAL}

class Factory(Daemon):
	"""
	The core factory class communicates with the Officeshots server and passes
	requests on to any of the available workers
	"""

	def configure(self, options):
		self.options = options
		self.config = ConfigParser.RawConfigParser()
		self.config.read(os.path.abspath(self.options.config_file))

		try:
			self.daemon = self.config.getboolean('global', 'daemon')
		except ConfigParser.NoOptionError as e:
			self.daemon = False

		if self.daemon and platform.system() == 'Windows':
			logging.error('Daemon mode is not supported on Windows. The factory will run in the foreground.')
			self.daemon = False

		# Configure logging
		if self.options.debug:
			logging.basicConfig(format = self.config.get('global', 'log_format'), level = logging.DEBUG)
			self.daemon = False
		else:
			level = LOGLEVELS.get(self.config.get('global', 'log_level'), logging.NOTSET)
			try:
				logging.basicConfig(
						format = self.config.get('global', 'log_format'),
						filename = self.config.get('global', 'log_file'),
						filemode = 'a',
						level = level
				)
			except IOError as e:
				print("Logfile IO error. Please make sure that the log_file setting is correct in config.ini")
				sys.exit(1)

		# Set the pidfile
		try:
			self.pidfile = self.config.get('global', 'pidfile')
		except ConfigParser.NoOptionError as e:
			if self.daemon:
				raise
			self.pidfile = '/tmp/pydaemon.pid'

		# Load factory name
		self.name = self.config.get('global', 'factory_name')

		# Configure the XMLRPC proxy
		self.transport_name = self.config.get('global', 'transport')

		# Make sure we're not using encrypted certificated in daemon mode with PySSL
		if self.transport_name == 'pyssl' and self.daemon:
			encrypted = (
				self.is_encrypted(self.config.get('global', 'tls_key_file')) or
				self.is_encrypted(self.config.get('global', 'tls_certificate_file'))
			)

			if encrypted:
				logging.critical("You cannot use encrypted PEM certificates in the PySSL transport when running as a daemon")
				return False

		return True

	def load_components(self):
                if options.indoc != '' and options.outdoc != '':
                        print("not loading certificates for local conversion job")
                else:

                        endpoint = self.config.get('global', 'xmlrpc_endpoint')
                        keyfile  = self.config.get('global', 'tls_key_file')
                        certfile = self.config.get('global', 'tls_certificate_file')

                        print "key:", keyfile
                        print "cert:", certfile
                        
                        ctx = ssl.create_default_context()

                        # This lets the user explicitly ignore server
                        # certificate verification so that keys that
                        # have just expired can still be used.
                        if self.options.skip_certificate_checks:
                                ctx.check_hostname = False
                                ctx.verify_mode = ssl.CERT_NONE

                        # As of August 2015 pyssl is internal and
                        # requires no Transports backend. This is
                        # tested on Python 2.7.10
                        if self.transport_name == "pyssl":
                                try:
                                        ctx.load_cert_chain( certfile, keyfile )
                                except ssl.SSLError as e:
                                        logging.critical('Error unlocking your certificate file: %s' % e.reason)
                                        sys.exit(1)
                                except IOError as e:
                                        logging.critical('Error unlocking your certificate file: %s' % e.strerror)
                                        sys.exit(1)
                                self.proxy = ServerProxy( endpoint, verbose=self.options.debug, context=ctx )
                        else:
                                transport = self.load('transports.' + self.transport_name, 'SSLTransport')
                                if transport is None:
                                        print("Transport %s could not be loaded" % self.transport_name)
                                        sys.exit(1)
                                transport = transport( keyfile, certfile )
                                self.proxy = ServerProxy( endpoint, transport=transport, verbose=self.options.debug)
                        

		# Load all the backends
                self.backends = []
                sections = [s.strip() for s in self.config.get('global', 'backends').split(',')]

                for section in sections:
                        if len(section) == 0:
                                continue

                        backend_name = self.config.get(section, 'backend')
                        if backend_name is None:
                                continue

                        backend = self.load('backends.' + backend_name.lower(), backend_name)
                        if backend is None:
                                continue

                        backend = backend(self.options, self.config, section)
                        try:
                                backend.initialize()
                        except BackendException as e:
                                logging.warning('Error initializing backend %s for %s: ' + str(e), backend_name, section)
                                continue

                        self.backends.append(backend)

                if len(self.backends) == 0:
                        logging.critical('No backends could be loaded')
                        return False

                # Configuration succeeded
                return True

	def load(self, package, class_name):
		"""
		A convenience function to import class_name from package
		"""
		try:
			module = __import__(package, globals(), locals(), class_name)
		except ImportError as e:
			logging.warning('Error importing %s from %s. ' + str(e), class_name, package)
			return None
		
		return getattr(module, class_name)

	def is_encrypted(self, key):
		"""
		Check if a PEM file is encrypted
		"""
		infile = open(key, "r")
		keytext = infile.read()
		infile.close()

		if keytext.find("ENCRYPTED") > -1:
			del keytext
			return True

		del keytext
		return False

	def systemload(self):
		"""
		Return the average system load
		"""
		try:
			return max(os.getloadavg())
		except (AttributeError, OSError):
			return None

	def loop(self):
		"""
		A single iteration of the main loop.
		Return False to terminate the application
		"""
		# Keep an eye on system load
		load = self.systemload()
		maxload = self.config.getfloat('global', 'load_max')
		if load > maxload:
			logging.debug("Systemload %.2f exceeds limit %.2f. Sleeping." % (load, maxload))
			time.sleep(60)
			return True

		# Poll for a job. Sleep for a minute if there's no work
		try:
			job = self.proxy.jobs.poll(self.name)
		except socket.error as ex:
			logging.warning(ex)
			logging.warning("Cannot connect to server. Sleeping.")
			time.sleep(60)
			return True
		except Fault as ex:
			logging.error("XML-RPC fault. Poll failed. Sleeping.")
			time.sleep(60)
			return True
		except ExpatError as ex:
			logging.error("XML parsing fault. Server probably sent an error in HTML. Poll failed. Sleeping.")
			time.sleep(60)
			return True

		if len(job) == 0:
			logging.debug('No jobs found. Sleeping.')
			time.sleep(60)
			return True

		# We have work. Find a backend to pass it off to
		for backend in self.backends:
			if backend.can_process(job):
				try:
					(format, document) = backend.process(job)
				except BackendException as ex:
					logging.warning(ex)
					if not ex.recoverable:
						logging.warning('Removing backend')
						self.backends.remove(backend)
						if len(self.backends) == 0:
							logging.critical('No more active backends.')
							return False
					time.sleep(60)
					return True

				try:
					self.proxy.jobs.finish(self.name, job['job'], format, document)
				except socket.error as ex:
					logging.warning("Cannot connect to server. Job cannot be finished. Sleeping.")
					time.sleep(60)
					return True
				except Fault as ex:
					logging.error("XML-RPC fault. Finishing failed. Sleeping.")
					time.sleep(60)
					return True
				except ExpatError as ex:
					logging.error("XML parsing fault. Server probably sent an error in HTML. Poll failed. Sleeping.")
					time.sleep(60)
					return True

				logging.info('Processed job %s', job['job'])
				return True

		logging.warning('No suitable backend found for job')
		logging.debug('Application: %s, version: %s, doctype: %s, format: %s' % (job['application'], job['version'], job['doctype'], job['format']))

		# TODO: Do something smart about that, like deactivating the related worker on the server
		return True

	def sigterm(self, signum, frame):
		logging.info('Stopping factory server.')
		Daemon.sigterm(self, signum, frame)


	def run(self):
		"""
		This is the main execution loop
		"""
		logging.info('Starting factory server.')
		try:
			while self.loop():
				pass
		except SystemExit as e:
			pass
		except:
			logging.critical(traceback.format_exc())
			sys.exit(1)
	
	def save(self,fn,data):
                fp = open(fn,"wb")
                fp.write(data)
                fp.close()
                        
	def local_conversion(self, tool, indoc, outdoc ):
                print("Doing a local document conversion...")
                print(" input:" + indoc)
                print(" output:" + outdoc)
                print("self.backends:",  self.backends)
                job = {}
                job['job'] = '1'
                job['application'] = self.config.get( tool, 'application' )                
                job['version']     = self.config.get( tool, 'version' )                
                job['doctype']     = 'odt'
                job['format']      = 'odf'
                if outdoc.endswith('.pdf'):
                        job['format']      = 'pdf'
                if outdoc.endswith('.png'):
                        job['format']      = 'png'
                job['filename']    = indoc
                job['document']    = base64.b64encode(open(indoc, 'rb').read())
                        
                for backend in self.backends:
                        print("backend:", backend)
                        if backend.can_process(job):
                                print("processing...")
                                (format, document) = backend.process(job)
                                print("saving to outdoc:" + outdoc)
                                self.save( outdoc, base64.b64decode(document) )
                return 0


if __name__ == "__main__":
        global scriptPath
        scriptPath = os.path.dirname(os.path.realpath(__file__))
        
	parser = OptionParser(usage='Usage: %prog [options] start|stop|restart')
	parser.add_option('-c', '--config-file', action='store', type='string', dest='config_file',
			default='../conf/config.ini', help='Full path to the configuration file to read.')
	parser.add_option('-d', '--debug', action='store_true', dest='debug',
			help='When in debug mode all errors will be written to the console and logging will be set to debug.')
	parser.add_option('-i', '--indoc', action='store', type='string', dest='indoc',
			default='', help='Full path to document to convert (see --outdoc).')
	parser.add_option('-o', '--outdoc', action='store', type='string', dest='outdoc',
			default='', help='Full path to where to store result of conversion (see --indoc).')
	parser.add_option('-t', '--tool', action='store', type='string', dest='tool',
			  default='', help='section name from config.ini for office suite to use, for example abiword')
	parser.add_option('', '--skip-certificate-checks', action='store_true', dest='skip_certificate_checks',
			  help='WARNING: Insecure! Do not verify the server certificate.')
	(options, args) = parser.parse_args()

        if options.skip_certificate_checks:
                msg = "WARNING! Not verifying the server certificate."
		logging.critical(msg)
                print msg

        # allow docs to be resolved relative to the current working
        # directory. Then set CWD to be where the script itself is located.
        if options.indoc != '':
                options.indoc = os.path.abspath(options.indoc)
        if options.outdoc != '':
                options.outdoc = os.path.abspath(options.outdoc)
        os.chdir(scriptPath)

                
	factory = Factory()
	if not factory.configure(options):
		logging.critical("Failed to load and parse the configuration")
		sys.exit(1)

	if options.indoc != '' and options.outdoc != '':
		if not factory.load_components():
			sys.exit(1)
		if options.tool == '':
		        logging.critical("Please supply the name of the --tool you would like to use")
		        sys.exit(1)
                        
		rc = factory.local_conversion( options.tool, options.indoc, options.outdoc )
		sys.exit(rc)
                
                
	if len(args) == 0 or args[0] == 'start':
		if not factory.load_components():
			sys.exit(1)

		if factory.daemon:
			factory.start()
		else:
			factory.run()
	                
	elif args[0] == 'stop':
		logging.info('Stopping factory server.')
		factory.stop()

	elif args[0] == 'restart':
		logging.info('Retarting factory server.')
		if not factory.load_components():
			sys.exit(1)

		factory.restart()
	else:
		print("Unknown command: %s" % args[0])
		sys.exit(2)
