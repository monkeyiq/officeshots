#!/usr/bin/env python
# Officeshots.org - Test your office documents in different applications
# Copyright (C) 2015 Ben Martin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
  Setup and update configuration for workers based on what is on the machine
"""

import os
import sys
import time
import logging
import ConfigParser
import os.path
import re
import time
import subprocess
import glob
import urllib 
import httplib2
import platform

from optparse import OptionParser
from shutil import copyfile
from distutils import spawn
from collections import namedtuple

SniffableApp = namedtuple("SniffableApp", "command detectedTool")
scriptPath = ""

def isWindows():
    return platform.system() == "Windows";

class ConfigureBackend:
    """
    Load, update, and save the configuration file for this factory, making changes
    based on what applications are on the local machine.
    """
    
    def __init__(self):
        self.data = []
        self.headers = {}
        self.officeAppToID = {}
        # FIXME: These should be loaded from the server to permit it to change.
        self.officeAppToID['ability office'] = '4aeecc92-b838-4b38-bcc8-445bc0a80105'
        self.officeAppToID['abiword'] = '496dd376-376c-4a8d-be75-5492c0a80105'
        self.officeAppToID['acrobat.com'] = '4aeeccb5-d11c-4b51-8c79-445bc0a80105'
        self.officeAppToID['apache openoffice'] = '547e25d2-36c4-4f3d-bf0c-4fc39e24bffa'
        self.officeAppToID['aspose.cells for .net'] = '54aa2d91-7d38-4c45-8295-03619e24bffa'
        self.officeAppToID['aspose.slides for .net'] = '54911a4c-a180-4d96-8943-73d49e24bffa'
        self.officeAppToID['aspose.words for .net'] = '4aeeccc1-3c8c-48af-81fc-445bc0a80105'
        self.officeAppToID['atlantis'] = '4b97ca5d-e1d8-4c71-8954-1ebdc1acb699'
        self.officeAppToID['calligra suite'] = '4eaf3260-907c-40f1-8f49-42f19e24bffa'
        self.officeAppToID['celframe office'] = '4aeeccf5-e91c-4778-b613-5fc2c0a80105'
        self.officeAppToID['corel wordperfect office'] = '4acf232d-78a4-4fb0-8631-1e71c0a80105'
        self.officeAppToID['digidna fileapp'] = '4aeecd38-1960-408b-806c-1816c0a80105'
        self.officeAppToID['editgrid'] = '4aeecd50-d890-40e9-95b5-632dc0a80105'
        self.officeAppToID['eioffice'] = '4aeecd62-d4bc-4e62-a387-632dc0a80105'
        self.officeAppToID['ethercalc'] = '547e2ef4-c084-4a68-9bad-50179e24bffa'
        self.officeAppToID['eurooffice'] = '4aeecd86-ce6c-41f6-ac96-632dc0a80105'
        self.officeAppToID['feng office'] = '4aeecdc9-dd94-44c4-aba3-632dc0a80105'
        self.officeAppToID['gnumeric'] = '4974af37-0018-4f89-9505-7973c0a80105'
        self.officeAppToID['go-oo'] = '49e47b77-f3ac-4cd2-8047-3d8ec0a80105'
        self.officeAppToID['goffice'] = '4aeecdd6-7a50-4a65-b6e7-632dc0a80105'
        self.officeAppToID['google apps'] = '4aeecdef-5cb0-471e-9651-5ea5c0a80105'
        self.officeAppToID['koffice'] = '4aeece2a-5ffc-4fc9-8ae1-5ea5c0a80105'
        self.officeAppToID['libreoffice'] = '4cb2cc8c-a488-43bc-977b-48809e24bffa'
        self.officeAppToID['lotus symphony'] = '4aeece56-beec-4e38-a948-5ea5c0a80105'
        self.officeAppToID['microsoft office'] = '4aeece72-4024-4ed5-b741-5e82c0a80105'
        self.officeAppToID['microsoft office (odf-converter addin)'] = '4aeece80-a878-4e7e-b11d-5e82c0a80105'
        self.officeAppToID['microsoft office (sun plugin)'] = '4aeece8a-91d4-46f3-bf14-5e82c0a80105'
        self.officeAppToID['neooffice'] = '4aeecf33-1c10-4e0a-b734-6a72c0a80105'
        self.officeAppToID['office reader'] = '498feee4-ac00-4834-8054-2918c0a80105'
        self.officeAppToID['opengoo'] = '4aeecf5e-0524-4931-9a3d-6a72c0a80105'
        self.officeAppToID['openoffice.org'] = '4974af01-4c9c-40ab-ac41-797fc0a80105'
        self.officeAppToID['redoffice'] = '49afa355-d204-4db9-a885-094ac0a80105'
        self.officeAppToID['softmaker office'] = '4aeed02b-09c8-46a8-87da-6a72c0a80105'
        self.officeAppToID['staroffice'] = '4aeecfb7-f238-497c-b0bb-5c62c0a80105'
        self.officeAppToID['textmaker viewer'] = '4aeed040-0128-412a-9aba-6a72c0a80105'
        self.officeAppToID['visor odf'] = '4aeed07b-b608-437f-a237-632dc0a80105'
        self.officeAppToID['webodf'] = '4d903c1e-13e0-444d-8974-50409e24bffa'
        self.officeAppToID['yozo office'] = '547e2fd8-e898-4be9-9aba-1b129e24bffa'
        self.officeAppToID['zcubes'] = '4aeecff8-3654-4bb4-9bce-5ea5c0a80105'
        self.officeAppToID['zoho'] = '4aeecff0-350c-4047-ae6e-5ea5c0a80105'
    
    def configure(self, options):
        self.options = options
        self.config = ConfigParser.RawConfigParser()
        self.config.read(os.path.abspath(self.options.config_file))
        if self.options.username:
            self.options.username = urllib.quote_plus(self.options.username)
        if self.options.password:
            self.options.password = urllib.quote_plus(self.options.password)
        return True

    def save(self):
        fp = open(self.options.config_file,"w")
        self.config.write(fp)
        fp.close()
        
    def ensureConfigSection(self,detectedTool):
        if not self.config.has_section(detectedTool) or not self.config.has_option( detectedTool, 'command' ):
            if not self.config.has_section(detectedTool):
                self.config.add_section(detectedTool)
            self.config.set( detectedTool, 'command', '' )
            self.config.set( detectedTool, 'doctype', 'odt' )
            self.config.set( detectedTool, 'formats', 'odf,pdf' )
            self.config.set( detectedTool, 'backend', 'CLI' )
            self.config.set( detectedTool, 'result',  '%temp_dir/%jobid_result.%dest_ext' )
            self.config.set( detectedTool, 'default_format', 'pdf' )
        
    def updateConfigCLI( self, detectedTool, toolVersion, appName, appPath, regexToChomp = '^[ ]*[^ ]+' ):
        print "updateConfigCLI() tool:", detectedTool, " ver:", toolVersion, " path:", appPath
        self.ensureConfigSection( detectedTool )

        self.config.set( detectedTool, 'version', toolVersion )
        self.config.set( detectedTool, 'application', appName )
        self.config.set( detectedTool, 'script_detected', '1' )
        self.config.set( detectedTool, 'script_detected_time', int(time.time()))
        
        cmdline = self.config.get( detectedTool, 'command' )
        print "CLI, regex:", regexToChomp
        print "CLI, before:", cmdline
        cmdline = re.sub(regexToChomp, "", cmdline)
        print "CLI, after:", cmdline
        cmdline = appPath + cmdline
        self.config.set( detectedTool, 'command', cmdline )
        self.workingBackends.append(detectedTool)

    def updateConfigOOO( self, detectedTool, toolVersion, appName, appPath ):
        print "updateConfigOOO() tool:", detectedTool, " ver:", toolVersion, " path:", appPath
        self.ensureConfigSection( detectedTool )

        self.config.set( detectedTool, 'version', toolVersion )
        self.config.set( detectedTool, 'application', appName )
        self.config.set( detectedTool, 'script_detected', '1' )
        self.config.set( detectedTool, 'script_detected_time', int(time.time()))
        self.config.set( detectedTool, 'backend', 'OOOServer' )
        self.config.set( detectedTool, 'ooo_host', 'localhost' )
        self.config.set( detectedTool, 'ooo_port', '8100' )
        self.config.remove_option( detectedTool, 'result' )
        cmdline = self.config.get( detectedTool, 'command' )
        cmdline = re.sub(" /.*/soffice ", " " + appPath + " ", cmdline)
        self.config.set( detectedTool, 'command', cmdline )
        
    def numberOnlyFromLastLine( self, s ):
        s = s.rstrip()
        s = s[s.rfind("\n")+1:]
        s = re.sub("[^0-9.]+", "", s)
        return s
    def numberOnlyFromFirstLine( self, s ):
        s = s.rstrip()
        s = s[0:s.find("\n")-1]
        s = re.sub("[^0-9.]+", "", s)
        return s
        
    def sniffApp(self,appName,appPath,detectedTool):
        
        """ It is important for appName to match what the server is expecting
            from it's applications table. Locally this is stored in self.officeAppToID """
        print "sniffApp() name:", appName, " path:", appPath

        toolVersion = ""
        pythonPrefix = []
        if isWindows():
            if re.search( 'unoconv', appPath ):
                pythonPrefix = ['python']

        print ("COMMAND:", (pythonPrefix + [appPath] + ["--help"] ))
        try:
            helptxt = subprocess.check_output(pythonPrefix + [appPath] + ["--help"], stderr=subprocess.STDOUT )
        except Exception as e:
            print "issue finding app:", appName, " error:", str(e)
            return
            
        #print helptxt
        if re.search("abiword", helptxt):
            xdetectedTool = "abiword"
            appName = "Abiword"
            toolVersion = subprocess.check_output([appPath, "--version"])
        if re.search("Calligra Document", helptxt):
            xdetectedTool = "calligra"
            appName = "Calligra"
            toolVersion = subprocess.check_output([appPath, "--version"])
            toolVersion = self.numberOnlyFromLastLine( toolVersion )
        if re.search("ssconvert", helptxt):
            xdetectedTool = "gnumeric"
            appName = "Gnumeric"
            toolVersion = subprocess.check_output([appPath, "--version"])
            toolVersion = self.numberOnlyFromFirstLine( toolVersion )
        if re.search("WebODF", helptxt):
            xdetectedTool = "webodf"
            appName = "WebODF"
            toolVersion = subprocess.check_output(pythonPrefix + [appPath] + ["--version"])
            toolVersion = self.numberOnlyFromLastLine( toolVersion )
            try:
                qtjsPath = spawn.find_executable("qtjsruntime")
            except Exception as e:
                print "issue finding qtjsruntime error:", str(e)
                return
            print "qtjsPath ", qtjsPath
            if qtjsPath is None:
                print "can not find qtjsruntime..."
                return
                
            webodfjsPath = os.path.dirname(os.path.realpath(qtjsPath)) + "/../../webodf/webodf.js"
            webodfjsPath = os.path.realpath(webodfjsPath)
            self.config.set( 'global', 'qtjspath', qtjsPath )
            self.config.set( 'global', 'webodfjspath', webodfjsPath )
            
            
        if re.search("Google Docs", helptxt):
            xdetectedTool = "googledocs"
            appName = "google apps"
            toolVersion = subprocess.check_output([appPath, "--version"])
            toolVersion = self.numberOnlyFromLastLine( toolVersion )
            gdrivePath = spawn.find_executable("drive-linux-x64")
            if gdrivePath is None:
                gdrivePath = spawn.find_executable("drive-linux-386")
            if gdrivePath is None:
                gdrivePath = spawn.find_executable("drive-linux-arm")
            if gdrivePath is None:
                gdrivePath = spawn.find_executable("drive-linux-rpi")
            if gdrivePath is None:
                return
            self.config.set( 'global', 'gdrivepath', gdrivePath )

        if re.search("unoconv", helptxt):
            libreOfficeRegexToChomp = '^.*unoconv["]*'
            xdetectedTool = "libreoffice4"
            appName = "LibreOffice"
            print("appPath:", appPath)
            toolVersion = subprocess.check_output(pythonPrefix + [appPath] + ["--version"])
            lover = 0
            appPathAugmented = ' '.join(pythonPrefix + [str('"' + appPath + '"')])
            if re.search( 'LibreOffice 4', toolVersion ):
                lover = 4
                detectedToolAugmented = re.sub('^libreoffice', 'libreoffice4', detectedTool)
                appName = "LibreOffice"
            elif re.search( 'LibreOffice 5', toolVersion ):
                lover = 5
                detectedToolAugmented = re.sub('^libreoffice', 'libreoffice5', detectedTool)
                appName = "LibreOffice"
            elif re.search( 'LibreOffice 3', toolVersion ):
                lover = 3
                detectedToolAugmented = re.sub('^libreoffice', 'libreoffice3', detectedTool)
                appName = "LibreOffice"
            else:
                print("No versions of LibreOffice were found")
                return

            toolVersion = self.numberOnlyFromLastLine( toolVersion )
            toolVersion = toolVersion.strip()
            self.updateConfigCLI( detectedToolAugmented, toolVersion, appName,
                                  appPathAugmented, libreOfficeRegexToChomp )

            if lover != 4:
                lopaths = glob.glob('/opt/libreoff*4*')
                if len(lopaths) != 0:
                    pythons = subprocess.check_output(['find'] + lopaths
                                                      + ['-type','f','-a','-name','python'])
                    pythons = pythons.rstrip().split('\n')
                    for py in pythons:
                        if py == '':
                            continue
                        print("py:", py )
                        toolVersion = subprocess.check_output([py,appPath,"--version"])
                        if re.search( 'LibreOffice 4', toolVersion ):
                            lover = 4
                            appName = "LibreOffice"
                            detectedToolAugmented = re.sub('^libreoffice', 'libreoffice4', detectedTool)
                            appPathAugmented = py + " " + appPath
                            toolVersion = self.numberOnlyFromLastLine( toolVersion )
                            toolVersion = toolVersion.strip()
                            print("------------------- appPathAugmented:", appPathAugmented)
                            print("------------------- appPath:", appPath)
                            self.updateConfigCLI( detectedToolAugmented, toolVersion, appName,
                                                  appPathAugmented, libreOfficeRegexToChomp )
            if lover != 5:
                lopaths = glob.glob('/opt/libreoff*5*')
                if len(lopaths) != 0:
                    pythons = subprocess.check_output(['find'] + lopaths
                                                      + ['-type','f','-a','-name','python'])
                    pythons = pythons.rstrip().split('\n')
                    for py in pythons:
                        if py == '':
                            continue
                        print("py:", py )
                        toolVersion = subprocess.check_output([py,appPath,"--version"])
                        if re.search( 'LibreOffice 5', toolVersion ):
                            lover = 5
                            appName = "LibreOffice"
                            detectedToolAugmented = re.sub('^libreoffice', 'libreoffice5', detectedTool)
                            appPathAugmented = py + " " + appPath
                            toolVersion = self.numberOnlyFromLastLine( toolVersion )
                            toolVersion = toolVersion.strip()
                            print("------------------- appPathAugmented:", appPathAugmented)
                            print("------------------- appPath:", appPath)
                            self.updateConfigCLI( detectedToolAugmented, toolVersion, appName,
                                                  appPathAugmented, libreOfficeRegexToChomp )
            return

        if detectedTool == "":
            detectedTool = xdetectedTool

        toolVersion = toolVersion.strip()
        if detectedTool == "":
            print "WARNING: Can't detect which tool is at ", appPath
            return
        
        print "detected:", detectedTool, " version:", toolVersion
        
        self.updateConfigCLI( detectedTool, toolVersion, appName, appPath )
        # self.config.get('global', 'factory_name')

    def sniffForLocalCertificate(self):
        certfile = self.config.get( 'global', 'tls_certificate_file' )
        print "certfile1:", certfile
        if not os.path.isfile( certfile ):
            certfile = os.path.realpath( scriptPath + "/../conf/cert.pem" )
            print "certfile2:", certfile
            if os.path.isfile( certfile ):
                print "updating tls_certificate_file to point to your key..."
                self.config.set( 'global', 'tls_certificate_file', certfile )
        certfile = self.config.get( 'global', 'tls_key_file' )
        if not os.path.isfile( certfile ):
            certfile = os.path.realpath( scriptPath + "/../conf/cert.pem" )
            if os.path.isfile( certfile ):
                print "updating tls_key_file to point to your key..."
                self.config.set( 'global', 'tls_key_file', certfile )
        return 0

    def sniffWord(self):
        pythonPrefix = ['python']
        try:
            versiontxt = subprocess.check_output([ "python", scriptPath + "/dochandlerscripts/try-get-windows-word-version.py"], stderr=subprocess.STDOUT )
            print "versiontxt:", versiontxt
        except Exception as e:
            print "issue finding Word, error:", str(e)
            return
        toolVersion = self.numberOnlyFromLastLine( versiontxt )
        detectedTool = "word"
        self.workingBackends.append(detectedTool)
        self.config.set( detectedTool, 'version', toolVersion )
        
        
    def setAppNotDetected(self,detectedTool):
        self.ensureConfigSection( detectedTool )
        self.config.set( detectedTool, 'script_detected', '0' )
        self.config.set( detectedTool, 'script_detected_time', int(time.time()))

    def resolveExecutablePath(self,program):
        # appPath = spawn.find_executable(appName)
        def is_exe(fpath):
            return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
        return None
 
    def sniff(self):
        print "config file at:", self.options.config_file
        print "factory name:", self.config.get('global', 'factory_name')
        print "sniffing!"
        self.workingBackends = []

        # The appName has to match the key from self.officeAppToID
        # because that is what the server gives us to find the app in our configuration.
        appList = [ SniffableApp("abiword",            "abiword")
#                    , SniffableApp("calligraconverter","calligra suite")
                    , SniffableApp("ssconvert",        "gnumeric")
                    , SniffableApp(scriptPath + "/dochandlerscripts/webodf/webodf-odf-to-exported.sh","webodf")
                    , SniffableApp("unoconv","libreoffice")
                    , SniffableApp(scriptPath + "/dochandlerscripts/google-docs-odt-to-exported.sh","googledocs")
                  ]
        for appName,detectedTool in appList:
            appPath = self.resolveExecutablePath(appName)
            if appPath is None:
                print "app ", appName, " not found"
                self.setAppNotDetected(detectedTool)
            else:
                self.sniffApp(appName,appPath,detectedTool)

        if isWindows():
            print "We are running on Microsoft Windows, so I'll check for Microsoft Word too."
            self.sniffWord()

        self.config.set( 'global', 'backends', ','.join(self.workingBackends))

        self.sniffForLocalCertificate()

        
        

    def authenticateWithOfficeShots(self):
        print "authenticateWithOfficeShots() user:", self.options.username, " pass:", self.options.password
        headers = { 'Accept-Language' : 'en-US,en;q=0.5',
                    'Accept-Encoding':'gzip, deflate',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Referer':'http://officeshots.org/',
                    'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',
                    'Connection': 'keep-alive'
        }
        h = httplib2.Http(".cache")
        self.h = h
        (resp_headers, content) = h.request("http://officeshots.org/users/login", "GET", headers=headers )
        print "resp_headers0:", resp_headers
        print "content0:",      content
        waffle = resp_headers['set-cookie'];
        waffle = re.sub(';.*','',waffle)
        headers = { 'Cookie': waffle, 'Accept-Language' : 'en-US,en;q=0.5',
                    'Accept-Encoding':'gzip, deflate',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Referer':'http://officeshots.org/',
                    'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',
                    'Connection': 'keep-alive'
        }
        self.headers = headers
        loginheaders = { 'Cookie': waffle, 'Accept-Language' : 'en-US,en;q=0.5',
                    'Accept-Encoding':'gzip, deflate',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Referer':'http://officeshots.org/users/login',
                    'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Connection': 'keep-alive'
        }
        
        
        loginbody = "_method=POST&data%5BUser%5D%5Bemail_address%5D=" + self.options.username + "&data%5BUser%5D%5Bpassword%5D=" + self.options.password
        (resp_headers, content) = h.request("http://officeshots.org/users/login",
                                            "POST", body=loginbody, headers=loginheaders )
        print "----------------------------------------------"
        print "----------------------------------------------"
        print "----------------------------------------------"
        print "resp_headers:", resp_headers
        print "content:",      content

        bodydata=''
        (resp_headers, content) = h.request("http://officeshots.org/users/view",
                                            "GET", headers=headers )
        print "----------------------------------------------"
        print "----------------------------------------------"
        print "----------------------------------------------"
        print "resp_headersV:", resp_headers
        print "contentV:",      content
        self.factoryNameToIDMap = self.userViewPageToFactoryNameToIDMap(content)
        return True

    def uploadconfig(self):
        print "uploadconfig() user:", self.options.username, " pass:", self.options.password
        factoryName = self.config.get( 'global', 'factory_name' )
        self.authenticateWithOfficeShots()
        if not self.factoryNameToIDMap.has_key(factoryName):
            print "uploadconfig() create the factory on the server first. FactoryName:", factoryName
            sys.exit(1)
        self.factoryID = self.factoryNameToIDMap[factoryName]
        self.setupWorkerNameToIDMap()
            
        factoryID = self.factoryNameToIDMap[factoryName]
        print "factory:", factoryName, " factoryID:", factoryID
        print "factoryNameToIDMap:", self.factoryNameToIDMap
        for backend in self.config.get( 'global', 'backends').split(','):
            print "backend:", backend
            self.configServerAddWorker( factoryName, factoryID, backend )



        
    def uploadconfigFNMapTest(self):
        pageText = """ <div class="related">
	<h3>Installed office applications</h3>
		<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th>Application</th>
		<th>Version</th>
		<th>Development</th>
		<th>Output formats</th>
		<th>Use for test suites</th>
					<th class="actions">Actions</th>
			</tr>
			<tr> """
        print "looking for worker ids..."
        fac = self.factoriesViewPageToWorkerNameToIDMap(pageText)
        print "fac:", fac

    def setupWorkerNameToIDMap(self):
        body=''
        (resp_headers, content) = self.h.request("http://officeshots.org/factories/view/"+self.factoryID,
                                                 "GET", body=body, headers=self.headers )
        print "----------------------------------------------"
        print "----------------------------------------------"
        print "----------------------------------------------"
        print "resp_headersV:", resp_headers
        print "contentV:",      content
        self.factoriesViewPageToWorkerNameToIDMap(content)

    def workerNameToIDMapContains( self, appName, appVer ):
        k = ( appName + "-" + appVer )
        print "workerNameToIDMapContains() key:", k
        print "self.WorkerNameToIDMap:", self.WorkerNameToIDMap
        return self.WorkerNameToIDMap.has_key( appName + "-" + appVer )
    
    def factoriesViewPageToWorkerNameToIDMap(self,pageText):
        ret = {}
        print "looking for worker ids..."
        print "pageText:", pageText
        str = pageText
        str = re.sub("\n", " ", str)
        str = re.sub("</tr>", "</tr>\n", str)
        str = re.sub(".*Installed office applications.*<table[^>]+>", "", str)
        for line in str.split('\n'):
            if re.match( '.*<th>.*', line ):
                continue
            print "line:", line
            m = re.match('\W*<tr>\W*<td>([^<]+)</td>\W+<td>([^<]+).*href="/workers/edit/([^"]+)".*', line )
            if m:
                wname = m.group(1)
                wver  = m.group(2)
                wid   = m.group(3)
                print "wname:", wname, " wver:", wver, " wid:", wid
                ret[wname.lower() + "-" + wver] = wid
        self.WorkerNameToIDMap = ret
        return ret

    def configServerAddWorker(self,factoryName,factoryID,detectedTool):
        appName = self.config.get( detectedTool, 'application' ).lower()
        applicationVersion = self.config.get( detectedTool, 'version' )
        applicationID = 'unknown'

        officeshotsAppName = appName
        if self.officeAppToID.has_key(appName):
            applicationID = self.officeAppToID[appName]
        if appName.startswith('libreoffice'):
            if self.officeAppToID.has_key('libreoffice'):
                applicationID = self.officeAppToID['libreoffice']
                officeshotsAppName = 'libreoffice'
        if appName.startswith('google'):
            if self.officeAppToID.has_key('google apps'):
                applicationID = self.officeAppToID['google apps']
                officeshotsAppName = 'google apps'

                
        if self.workerNameToIDMapContains( officeshotsAppName, applicationVersion ):
            print "Officeshots already has this worker ... appName:", appName, " version:", applicationVersion
            return
                
        print "configServerAddWorker factoryID         :", factoryID
        print "configServerAddWorker factoryName       :", factoryName
        print "configServerAddWorker applicationName   :", appName
        print "configServerAddWorker applicationID     :", applicationID
        print "configServerAddWorker applicationVersion:", applicationVersion

        doctype = 'data[Doctype][Doctype][]=4970527f-d0a0-4206-b718-7900c0a80105'
        if self.config.get( detectedTool, 'doctype' ) == 'ods':
            doctype = 'data[Doctype][Doctype][]=49705294-bfa8-4ee2-9cec-7900c0a80105'
        if self.config.get( detectedTool, 'doctype' ) == 'odt,ods':
            doctype = doctype + '&data[Doctype][Doctype][]=49705294-bfa8-4ee2-9cec-7900c0a80105'
        doctype = urllib.quote(doctype)

        body = "_method=POST&data[Worker][factory_id]={0}&data[Factory][name]={1}&data[Worker][application_id]={2}&data[Worker][version]={3}&data[Worker][development]=0&data[Worker][testsuite]=0&data[Doctype][Doctype]=&{4}&data[Format][Format]=&data[Format][Format][]=496de908-d92c-4e3b-9447-558fc0a80105&data[Format][Format][]=496de976-9a10-403c-aa90-55e6c0a80105"
        # encode the lot and then unencode the {} which are used by string.format() for
        # the replacement below.
        body = urllib.quote(body)
        body = body.replace("%7B", "{")
        body = body.replace("%7D", "}")
        body = body.format( factoryID, factoryName, applicationID, applicationVersion, doctype  )

        headers = self.headers
        headers['Content-Type'] = 'application/x-www-form-urlencoded'
        (resp_headers, content) = self.h.request("http://officeshots.org/workers/add",
                                                 "POST", body=body, headers=headers )
        print "resp_headers:", resp_headers
        print "content:", content
            
        

    def userViewPageToFactoryNameToIDMap(self,usersViewPage):
        factoryNameToIDMap = {}
        m = re.findall('href="/factories/view/.*</a>', usersViewPage )
        for mi in m:
            m = re.match('\W*href="/factories/view/([^"]+)">([^<]+)<.*', mi )
            if m:
                fid   = m.group(1)
                fname = m.group(2)
                factoryNameToIDMap[fname] = fid
        return factoryNameToIDMap
        
    def showAppTable(self):
        for key, value in self.officeAppToID.iteritems():
            print value, " ", key
        
            
        
            
if __name__ == "__main__":
    scriptPath = os.path.dirname(os.path.realpath(__file__))
    os.chdir(scriptPath)
    
    parser = OptionParser(usage='Usage: %prog [options]')
    parser.add_option('-c', '--config-file', action='store', type='string', dest='config_file',
                      default='../conf/config.ini', help='Full path to the configuration file to read.')
    parser.add_option('-d', '--debug', action='store_true', dest='debug',
                      help='When in debug mode all errors will be written to the console and logging will be set to debug.')
    parser.add_option('-s', '--sniff', action='store_true', dest='sniff',
                       default=False, help='Sniff around for office applications that can be used and setup cofnig file to use them.')
    parser.add_option('', '--cert-sniff', action='store_true', dest='certsniff',
                       default=False, help='Sniff around for SSL certificate to use in your config.ini.')
    parser.add_option('-u', '--uploadconfig', action='store_true', dest='uploadconfig',
                       default=False, help='Upload the info from the local config.ini to the OfficeShots.org machine.')
    parser.add_option('', '--username', action='store', type='string', dest='username',
                       default=False, help='Username to authenticate on OfficeShots.org machine.')    
    parser.add_option('', '--password', action='store', type='string', dest='password',
                       default=False, help='Password to authenticate on OfficeShots.org machine.')
    parser.add_option('', '--show-app-table', action='store_true', dest='showAppTable',
                       default=False, help='Show the appName and appID for all known applications on the server.')    
    (options, args) = parser.parse_args()
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)

    createdConfigIni = False
    
    if not os.path.isfile(options.config_file):
        templatefile = re.sub('\.ini$', '.default.ini', options.config_file)
        if os.path.isfile(templatefile):
            print "Your config file does not exist..."
            print "  but the template does so I am setting up from the template."
            copyfile(templatefile, options.config_file)
            createdConfigIni = True
        else:
            print "Your config file does not exist..."
            print "  no template is found either! Please setup the config.ini manually"
            sys.exit(1)
            
    obj = ConfigureBackend()
    if not obj.configure(options):
        logging.critical("Failed to load and parse the configuration")
        sys.exit(1)

    if createdConfigIni:
        if isWindows():
            print("running on Microsoft Windows, setting transport and logfile directories...")
            obj.config.set( 'global', 'transport', 'pyssl' )
            tmpdir = os.environ["TMP"]
            if tmpdir == "" or tmpdir == None:
                tmpdir = os.environ["TEMP"]
            tmpfiles = tmpdir + "/officeshots"
            obj.config.set( 'global', 'tmp_files', tmpfiles )
            logfile = tmpdir + "/officeshots.log"
            obj.config.set( 'global', 'log_file', logfile )

    if options.showAppTable:
        obj.showAppTable()
        sys.exit(0)
        
    if not options.sniff and options.certsniff:
        rc = obj.sniffForLocalCertificate()
        obj.save()
        sys.exit(rc)

    if options.sniff:
        rc = obj.sniff()
        obj.save()
        sys.exit(rc)

    if options.uploadconfig:
        rc = obj.uploadconfig()
        sys.exit(rc)

    # If we haven't done anything by now then the user has made an invalid choce.
    print "If you wish to update your local config with the Office Suites found"
    print "on your system then please use the --sniff option"
    print 
    print "we will use the config file at:", options.config_file
    sys.exit(1)
        
