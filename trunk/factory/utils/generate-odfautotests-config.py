#!/usr/bin/env python

import os
import sys
import time
import platform

import ConfigParser

def isWindows():
    return platform.system() == "Windows";



if __name__ == "__main__":
        global scriptPath
        scriptPath = os.path.dirname(os.path.realpath(__file__))
        os.chdir(scriptPath)

	config = ConfigParser.RawConfigParser()
	config.read(os.path.abspath("../conf/config.ini"))

        factoryPath = os.path.abspath("../src/factory.py")

        envExports = '''
				<env name="PATH" />
				<env name="DISPLAY" />
				<env name="ALLUSERSPROFILE" />
				<env name="APPDATA" />
				<env name="CLIENTNAME" />
				<env name="CommonProgramFiles" />
				<env name="CommonProgramFiles(x86)" />
				<env name="CommonProgramW6432" />
				<env name="COMPUTERNAME" />
				<env name="ComSpec" />
				<env name="FP_NO_HOST_CHECK" />
				<env name="HOMEDRIVE" />
				<env name="HOMEPATH" />
				<env name="JAVA_HOME" />
				<env name="LOCALAPPDATA" />
				<env name="LOGONSERVER" />
				<env name="NUMBER_OF_PROCESSORS" />
				<env name="OS" />
				<env name="Path" />
				<env name="PATHEXT" />
				<env name="PROCESSOR_ARCHITECTURE" />
				<env name="PROCESSOR_IDENTIFIER" />
				<env name="PROCESSOR_LEVEL" />
				<env name="PROCESSOR_REVISION" />
				<env name="ProgramData" />
				<env name="ProgramFiles" />
				<env name="ProgramFiles(x86)" />
				<env name="ProgramW6432" />
				<env name="PSModulePath" />
				<env name="PUBLIC" />
				<env name="SESSIONNAME" />
				<env name="SystemDrive" />
				<env name="SystemRoot" />
				<env name="TEMP" />
				<env name="TMP" />
				<env name="USERDOMAIN" />
				<env name="USERDOMAIN_ROAMINGPROFILE" />
				<env name="USERNAME" />
				<env name="USERPROFILE" />
				<env name="windir" />'''

        python = 'python'
	cmd = '			<command exe="' + factoryPath + '">' + envExports
        if isWindows():
	    cmd = '			<command exe="' + python + '">' + envExports
	    cmd = cmd + '\n				<argument value="' + factoryPath + '" />'
            

        print '<!--'
        print '      Generated with officeshots factory utils/generate-odfautotests-config.py     '
        print '      Generation time ' + time.ctime()
        print '-->'
        print '''<config xmlns="http://www.example.org/documenttests" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.example.org/documenttests documenttests.xsd">
'''
        
        for backend in config.get( 'global', 'backends').split(','):
#            print "backend:", backend
	    print '	<target name="' + backend + '">'
            print '''		<output outputType="odt1.2" inputTypes="odt1.0 odt1.1 odt1.2 odt1.2ext">'''
            print cmd
	    print '''
				<argument value="--tool" />
				<argument value="''' + backend + '''" />
				<argument value="-i" />
				<infile />
				<argument value="-o" />
				<outfile />
			</command>
		</output>
		<output outputType="pdf" inputTypes="odt1.0 odt1.1 odt1.2 odt1.2ext">'''
            print cmd
	    print '''
				<argument value="--tool" />
				<argument value="''' + backend + '''" />
				<argument value="-i" />
				<infile />
				<argument value="-o" />
				<outfile />
			</command>
		</output>
	</target>
'''


        print '</config>'
